Package: Glouton
Author: Tim Moore (moore@bricoworks.com)

BRIEF DESCRIPTION

Glouton is a Common Lisp package for programming in OpenGL, using
GLX. It includes a complete set of FFI bindings for X11 and 
OpenGL 1.5 that are defined using the CFFI package. It
should be portable to all the Common Lisp implementations supported
by CFFI. The names of X11 and OpenGL functions are taken directly
from the C names without preserving case. This follows the model of
Richard Mann's and Knut Arild Erstad's "nameless" OpenGL
bindings.

While Glouton can be used exclusively to interact with the C libraries
for X and OpenGL, it was written to be used with CLX, the native Lisp
binding for X11. Some synchronization is required between the two
connections to the X server, and doubtless we haven't found all the
cases yet.

Glouton will eventually have functionality on the level of Glut for
common OpenGL tasks, as well as utilities for things like arcball
controllers. Documentation needs to be written, but for the moment
look at examples/teapot-example.lisp to get started.
