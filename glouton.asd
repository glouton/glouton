;;; -*- Mode: Lisp -*-

;;; Copyright (c) 2005,2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :cl-user)


(defpackage :glouton-system
  (:use #:asdf #:cl))
(in-package :glouton-system)

(defsystem glouton
  :name "GLOUTON"
  :depends-on (:cffi :clx)
  :components
  ((:module :src
	    :components
	    ((:file "x-package")
	     (:file "gl-package")
	     (:file "glx-package" :depends-on ("gl-package" "x-package"))
	     (:file "package"
		    :depends-on ("gl-package" "x-package" "glx-package"))
	     (:file "load-libs" :depends-on ("package"))
	     (:file "x" :depends-on ("load-libs" "x-package"))
	     (:file "gl" :depends-on ("load-libs" "gl-package"))
	     (:file "glx" :depends-on ("load-libs" "glx-package" "x" "gl"))
	     (:file "extensions" :depends-on ("package" "glx"))
	     (:file "gl-ext-defs"
		    :depends-on ("load-libs" "package" "extensions"))
	     (:file "interface"
		    :depends-on ("package" "x" "gl" "glx"))
	     (:file "teapot"
		    :depends-on ("package" "gl"))
	     (:file "basic-application"
		    :depends-on ("package" "x" "interface"))
	     (:file "single-window-application"
		    :depends-on ("package" "x" "glx" "interface" "basic-application"))))))

(defsystem glouton-examples
  :name "GLOUTON-EXAMPLES"
  :depends-on (:glouton)
  :components
  ((:module :examples
	    :components
	    ((:file "package")
	     #-(and)(:file "bounce" :depends-on ("package"))
	     (:file "teapot-example" :depends-on ("package"))))))

