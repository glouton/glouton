(in-package :glouton)

;;; Copyright (c) 2005 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(defmacro define-function (names args &rest key-args)
  (let ((function-symbol (cadr names)))
    `(progn
       (declaim (inline ,function-symbol))
       (uffi:def-function ,names ,args ,@key-args))))

(in-package :uffi)

;;; Fix some problems with (* (* :char)) in structure definitions
#+openmcl
(defun %convert-from-uffi-type (type context)
  "Converts from a uffi type to an implementation specific type"
  (if (atom type)
      (cond
       #+(or allegro cormanlisp)
       ((and (or (eq context :routine) (eq context :return))
	     (eq type :cstring))
	(setq type '((* :char) integer)))
       #+(or cmu sbcl scl)
       ((eq context :type)
	(let ((cmu-type (gethash type *cmu-def-type-hash*)))
	  (if cmu-type
	      cmu-type
	      (basic-convert-from-uffi-type type))))
       #+lispworks
       ((and (eq context :return)
	     (eq type :cstring))
	(basic-convert-from-uffi-type :cstring-returning))
       #+openmcl
       ((and (eq context :struct) (eq type '*))
	'*)
       #+(and mcl (not openmcl))
       ((and (eq type :void) (eq context :return)) nil)
       (t
	(basic-convert-from-uffi-type type)))
    (let ((sub-type (car type)))
      (case sub-type
	(cl:quote
	 (convert-from-uffi-type (cadr type) context))
	(:struct-pointer
	 #+mcl `(:* (:struct ,(%convert-from-uffi-type (cadr type) :struct)))
	 #-mcl (%convert-from-uffi-type (list '* (cadr type)) :struct)
	 )
	(:struct
	 #+mcl `(:struct ,(%convert-from-uffi-type (cadr type) :struct))
	 #-mcl (%convert-from-uffi-type (cadr type) :struct)
	 )
       (:union
	#+mcl `(:union ,(%convert-from-uffi-type (cadr type) :union))
	#-mcl (%convert-from-uffi-type (cadr type) :union)
	)
       (t
	(cons (%convert-from-uffi-type (first type) context) 
	      (%convert-from-uffi-type (rest type) context)))))))

#-(and)
(defun convert-from-uffi-type (type context)
  (let ((result (%convert-from-uffi-type type context)))
    (cond
      #+openmcl
      ((and (eq result :address) (eq context :struct))
       '*)
      ((atom result) result)
      #+openmcl
      ((eq (car result) :address)
       (if (eq context :struct)
	   (append '(:*) (cdr result))
	   :address))
      #+(and mcl (not openmcl))
      ((and (eq (car result) :pointer) (eq context :allocation) :pointer))
      (t result))))