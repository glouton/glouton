;;; -*- Mode: Lisp -*-

;;; Copyright (c) 2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :glouton)

(defgeneric open-window (application &key width height event-mask
				     visual-attributes))

(defgeneric close-window (application window))

(defclass single-window-application (basic-application)
  ((app-window :accessor app-window :initform nil)
   (width :accessor width)
   (height :accessor height)
   (visual-attributes :accessor visual-attributes
		      :initarg :visual-attributes)
   (event-mask :accessor event-mask :initarg :event-mask)
   (redisplayp :accessor redisplayp :initform nil))
  (:default-initargs
    :visual-attributes (list GLX_RGBA
			     GLX_RED_SIZE 1
			     GLX_GREEN_SIZE 1
			     GLX_BLUE_SIZE 1
			     GLX_DEPTH_SIZE 16
			     GLX_DOUBLEBUFFER
			     glouton-x:None)
    :event-mask '(:structure-notify :exposure :button-press)))

(defmethod initialize ((application single-window-application))
  (setf (app-window application) (open-window application)))

(defmethod run-application ((application single-window-application)
			    &key host display (width 300) (height 300))
  (declare (ignore host display))
  (setf (width application) width)
  (setf (height application) height)

  (call-next-method))

(defmethod open-window ((application single-window-application)
                        &key (width (width application))
			(height (height application))
			(event-mask (event-mask application))
			(visual-attributes (visual-attributes application)))
  (open-gl-window (display application) width height event-mask
		  :visual-attributes visual-attributes))

(defmethod handle-destroy
    ((application single-window-application) display window)
  (declare (ignore display window))
  (exit-application application))

(defmethod handle-exposure ((application single-window-application) event-key
			    &key count)
  (declare (ignore event-key))
  (if (zerop count)
      (progn
	(setf (redisplayp application) t) ; Yes, I know this returns t...
	t)
      nil))

;;; Is this right?
(defmethod close-window ((application single-window-application) window)
  (process-gl-events (display application) :ignore t)
  (destroy-gl-window window)
  (setf (app-window application) nil))

(defmethod cleanup ((application single-window-application))
  (when (app-window application)
    (close-window application (app-window application))))

(defmethod refresh ((application single-window-application) timeout-happened)
  (declare (ignore timeout-happened))
  nil)

(defmethod refresh :around
    ((application single-window-application) timeout-happened)
  (if (or (redisplayp application) timeout-happened)
      (call-next-method))
  (setf (redisplayp application) nil))
