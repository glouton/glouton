(in-package :glouton)

(define-extension :gl_version_1_4
  (("glBlendFuncSeparate" glBlendFuncSeparate) :void GLenum GLenum GLenum
   GLenum)
  (("glFogCoordf" glFogCoordf) :void GLfloat)
  (("glFogCoordfv" glFogCoordfv) :void :pointer)
  (("glFogCoordd" glFogCoordd) :void GLdouble)
  (("glFogCoorddv" glFogCoorddv) :void :pointer)
  (("glFogCoordPointer" glFogCoordPointer) :void GLenum GLsizei :pointer)
  (("glMultiDrawArrays" glMultiDrawArrays) :void GLenum :pointer :pointer
   GLsizei)
  (("glMultiDrawElements" glMultiDrawElements) :void GLenum :pointer GLenum
   :pointer GLsizei)
  (("glPointParameterf" glPointParameterf) :void GLenum GLfloat)
  (("glPointParameterfv" glPointParameterfv) :void GLenum :pointer)
  (("glPointParameteri" glPointParameteri) :void GLenum GLint)
  (("glPointParameteriv" glPointParameteriv) :void GLenum :pointer)
  (("glSecondaryColor3b" glSecondaryColor3b) :void GLbyte GLbyte GLbyte)
  (("glSecondaryColor3bv" glSecondaryColor3bv) :void :pointer)
  (("glSecondaryColor3d" glSecondaryColor3d) :void GLdouble GLdouble GLdouble)
  (("glSecondaryColor3dv" glSecondaryColor3dv) :void :pointer)
  (("glSecondaryColor3f" glSecondaryColor3f) :void GLfloat GLfloat GLfloat)
  (("glSecondaryColor3fv" glSecondaryColor3fv) :void :pointer)
  (("glSecondaryColor3i" glSecondaryColor3i) :void GLint GLint GLint)
  (("glSecondaryColor3iv" glSecondaryColor3iv) :void :pointer)
  (("glSecondaryColor3s" glSecondaryColor3s) :void GLshort GLshort GLshort)
  (("glSecondaryColor3sv" glSecondaryColor3sv) :void :pointer)
  (("glSecondaryColor3ub" glSecondaryColor3ub) :void GLubyte GLubyte GLubyte)
  (("glSecondaryColor3ubv" glSecondaryColor3ubv) :void :pointer)
  (("glSecondaryColor3ui" glSecondaryColor3ui) :void GLuint GLuint GLuint)
  (("glSecondaryColor3uiv" glSecondaryColor3uiv) :void :pointer)
  (("glSecondaryColor3us" glSecondaryColor3us) :void GLushort GLushort
   GLushort)
  (("glSecondaryColor3usv" glSecondaryColor3usv) :void :pointer)
  (("glSecondaryColorPointer" glSecondaryColorPointer) :void GLint GLenum
   GLsizei :pointer)
  (("glWindowPos2d" glWindowPos2d) :void GLdouble GLdouble)
  (("glWindowPos2dv" glWindowPos2dv) :void :pointer)
  (("glWindowPos2f" glWindowPos2f) :void GLfloat GLfloat)
  (("glWindowPos2fv" glWindowPos2fv) :void :pointer)
  (("glWindowPos2i" glWindowPos2i) :void GLint GLint)
  (("glWindowPos2iv" glWindowPos2iv) :void :pointer)
  (("glWindowPos2s" glWindowPos2s) :void GLshort GLshort)
  (("glWindowPos2sv" glWindowPos2sv) :void :pointer)
  (("glWindowPos3d" glWindowPos3d) :void GLdouble GLdouble GLdouble)
  (("glWindowPos3dv" glWindowPos3dv) :void :pointer)
  (("glWindowPos3f" glWindowPos3f) :void GLfloat GLfloat GLfloat)
  (("glWindowPos3fv" glWindowPos3fv) :void :pointer)
  (("glWindowPos3i" glWindowPos3i) :void GLint GLint GLint)
  (("glWindowPos3iv" glWindowPos3iv) :void :pointer)
  (("glWindowPos3s" glWindowPos3s) :void GLshort GLshort GLshort)
  (("glWindowPos3sv" glWindowPos3sv) :void :pointer))
(define-extension :gl_version_1_5
  (("glGenQueries" glGenQueries) :void GLsizei :pointer)
  (("glDeleteQueries" glDeleteQueries) :void GLsizei :pointer)
  (("glIsQuery" glIsQuery) GLboolean GLuint)
  (("glBeginQuery" glBeginQuery) :void GLenum GLuint)
  (("glEndQuery" glEndQuery) :void GLenum)
  (("glGetQueryiv" glGetQueryiv) :void GLenum GLenum :pointer)
  (("glGetQueryObjectiv" glGetQueryObjectiv) :void GLuint GLenum :pointer)
  (("glGetQueryObjectuiv" glGetQueryObjectuiv) :void GLuint GLenum :pointer)
  (("glBindBuffer" glBindBuffer) :void GLenum GLuint)
  (("glDeleteBuffers" glDeleteBuffers) :void GLsizei :pointer)
  (("glGenBuffers" glGenBuffers) :void GLsizei :pointer)
  (("glIsBuffer" glIsBuffer) GLboolean GLuint)
  (("glBufferData" glBufferData) :void GLenum GLsizeiptr :pointer GLenum)
  (("glBufferSubData" glBufferSubData) :void GLenum GLintptr GLsizeiptr
   :pointer)
  (("glGetBufferSubData" glGetBufferSubData) :void GLenum GLintptr GLsizeiptr
   :pointer)
  (("glMapBuffer" glMapBuffer) :pointer GLenum GLenum)
  (("glUnmapBuffer" glUnmapBuffer) GLboolean GLenum)
  (("glGetBufferParameteriv" glGetBufferParameteriv) :void GLenum GLenum
   :pointer)
  (("glGetBufferPointerv" glGetBufferPointerv) :void GLenum GLenum :pointer))
(define-extension :gl_arb_multitexture
  (("glActiveTextureARB" glActiveTextureARB) :void GLenum)
  (("glClientActiveTextureARB" glClientActiveTextureARB) :void GLenum)
  (("glMultiTexCoord1dARB" glMultiTexCoord1dARB) :void GLenum GLdouble)
  (("glMultiTexCoord1dvARB" glMultiTexCoord1dvARB) :void GLenum :pointer)
  (("glMultiTexCoord1fARB" glMultiTexCoord1fARB) :void GLenum GLfloat)
  (("glMultiTexCoord1fvARB" glMultiTexCoord1fvARB) :void GLenum :pointer)
  (("glMultiTexCoord1iARB" glMultiTexCoord1iARB) :void GLenum GLint)
  (("glMultiTexCoord1ivARB" glMultiTexCoord1ivARB) :void GLenum :pointer)
  (("glMultiTexCoord1sARB" glMultiTexCoord1sARB) :void GLenum GLshort)
  (("glMultiTexCoord1svARB" glMultiTexCoord1svARB) :void GLenum :pointer)
  (("glMultiTexCoord2dARB" glMultiTexCoord2dARB) :void GLenum GLdouble
   GLdouble)
  (("glMultiTexCoord2dvARB" glMultiTexCoord2dvARB) :void GLenum :pointer)
  (("glMultiTexCoord2fARB" glMultiTexCoord2fARB) :void GLenum GLfloat GLfloat)
  (("glMultiTexCoord2fvARB" glMultiTexCoord2fvARB) :void GLenum :pointer)
  (("glMultiTexCoord2iARB" glMultiTexCoord2iARB) :void GLenum GLint GLint)
  (("glMultiTexCoord2ivARB" glMultiTexCoord2ivARB) :void GLenum :pointer)
  (("glMultiTexCoord2sARB" glMultiTexCoord2sARB) :void GLenum GLshort GLshort)
  (("glMultiTexCoord2svARB" glMultiTexCoord2svARB) :void GLenum :pointer)
  (("glMultiTexCoord3dARB" glMultiTexCoord3dARB) :void GLenum GLdouble GLdouble
   GLdouble)
  (("glMultiTexCoord3dvARB" glMultiTexCoord3dvARB) :void GLenum :pointer)
  (("glMultiTexCoord3fARB" glMultiTexCoord3fARB) :void GLenum GLfloat GLfloat
   GLfloat)
  (("glMultiTexCoord3fvARB" glMultiTexCoord3fvARB) :void GLenum :pointer)
  (("glMultiTexCoord3iARB" glMultiTexCoord3iARB) :void GLenum GLint GLint
   GLint)
  (("glMultiTexCoord3ivARB" glMultiTexCoord3ivARB) :void GLenum :pointer)
  (("glMultiTexCoord3sARB" glMultiTexCoord3sARB) :void GLenum GLshort GLshort
   GLshort)
  (("glMultiTexCoord3svARB" glMultiTexCoord3svARB) :void GLenum :pointer)
  (("glMultiTexCoord4dARB" glMultiTexCoord4dARB) :void GLenum GLdouble GLdouble
   GLdouble GLdouble)
  (("glMultiTexCoord4dvARB" glMultiTexCoord4dvARB) :void GLenum :pointer)
  (("glMultiTexCoord4fARB" glMultiTexCoord4fARB) :void GLenum GLfloat GLfloat
   GLfloat GLfloat)
  (("glMultiTexCoord4fvARB" glMultiTexCoord4fvARB) :void GLenum :pointer)
  (("glMultiTexCoord4iARB" glMultiTexCoord4iARB) :void GLenum GLint GLint GLint
   GLint)
  (("glMultiTexCoord4ivARB" glMultiTexCoord4ivARB) :void GLenum :pointer)
  (("glMultiTexCoord4sARB" glMultiTexCoord4sARB) :void GLenum GLshort GLshort
   GLshort GLshort)
  (("glMultiTexCoord4svARB" glMultiTexCoord4svARB) :void GLenum :pointer))
(define-extension :gl_arb_transpose_matrix
  (("glLoadTransposeMatrixfARB" glLoadTransposeMatrixfARB) :void :pointer)
  (("glLoadTransposeMatrixdARB" glLoadTransposeMatrixdARB) :void :pointer)
  (("glMultTransposeMatrixfARB" glMultTransposeMatrixfARB) :void :pointer)
  (("glMultTransposeMatrixdARB" glMultTransposeMatrixdARB) :void :pointer))
(define-extension :gl_arb_multisample
  (("glSampleCoverageARB" glSampleCoverageARB) :void GLclampf GLboolean))
(define-extension :gl_arb_texture_env_add)
(define-extension :gl_arb_texture_cube_map)
(define-extension :gl_arb_texture_compression
  (("glCompressedTexImage3DARB" glCompressedTexImage3DARB) :void GLenum GLint
   GLenum GLsizei GLsizei GLsizei GLint GLsizei :pointer)
  (("glCompressedTexImage2DARB" glCompressedTexImage2DARB) :void GLenum GLint
   GLenum GLsizei GLsizei GLint GLsizei :pointer)
  (("glCompressedTexImage1DARB" glCompressedTexImage1DARB) :void GLenum GLint
   GLenum GLsizei GLint GLsizei :pointer)
  (("glCompressedTexSubImage3DARB" glCompressedTexSubImage3DARB) :void GLenum
   GLint GLint GLint GLint GLsizei GLsizei GLsizei GLenum GLsizei :pointer)
  (("glCompressedTexSubImage2DARB" glCompressedTexSubImage2DARB) :void GLenum
   GLint GLint GLint GLsizei GLsizei GLenum GLsizei :pointer)
  (("glCompressedTexSubImage1DARB" glCompressedTexSubImage1DARB) :void GLenum
   GLint GLint GLsizei GLenum GLsizei :pointer)
  (("glGetCompressedTexImageARB" glGetCompressedTexImageARB) :void GLenum GLint
   :pointer))
(define-extension :gl_arb_texture_border_clamp)
(define-extension :gl_arb_point_parameters
  (("glPointParameterfARB" glPointParameterfARB) :void GLenum GLfloat)
  (("glPointParameterfvARB" glPointParameterfvARB) :void GLenum :pointer))
(define-extension :gl_arb_vertex_blend
  (("glWeightbvARB" glWeightbvARB) :void GLint :pointer)
  (("glWeightsvARB" glWeightsvARB) :void GLint :pointer)
  (("glWeightivARB" glWeightivARB) :void GLint :pointer)
  (("glWeightfvARB" glWeightfvARB) :void GLint :pointer)
  (("glWeightdvARB" glWeightdvARB) :void GLint :pointer)
  (("glWeightubvARB" glWeightubvARB) :void GLint :pointer)
  (("glWeightusvARB" glWeightusvARB) :void GLint :pointer)
  (("glWeightuivARB" glWeightuivARB) :void GLint :pointer)
  (("glWeightPointerARB" glWeightPointerARB) :void GLint GLenum GLsizei
   :pointer)
  (("glVertexBlendARB" glVertexBlendARB) :void GLint))
(define-extension :gl_arb_matrix_palette
  (("glCurrentPaletteMatrixARB" glCurrentPaletteMatrixARB) :void GLint)
  (("glMatrixIndexubvARB" glMatrixIndexubvARB) :void GLint :pointer)
  (("glMatrixIndexusvARB" glMatrixIndexusvARB) :void GLint :pointer)
  (("glMatrixIndexuivARB" glMatrixIndexuivARB) :void GLint :pointer)
  (("glMatrixIndexPointerARB" glMatrixIndexPointerARB) :void GLint GLenum
   GLsizei :pointer))
(define-extension :gl_arb_texture_env_combine)
(define-extension :gl_arb_texture_env_crossbar)
(define-extension :gl_arb_texture_env_dot3)
(define-extension :gl_arb_texture_mirrored_repeat)
(define-extension :gl_arb_depth_texture)
(define-extension :gl_arb_shadow)
(define-extension :gl_arb_shadow_ambient)
(define-extension :gl_arb_window_pos
  (("glWindowPos2dARB" glWindowPos2dARB) :void GLdouble GLdouble)
  (("glWindowPos2dvARB" glWindowPos2dvARB) :void :pointer)
  (("glWindowPos2fARB" glWindowPos2fARB) :void GLfloat GLfloat)
  (("glWindowPos2fvARB" glWindowPos2fvARB) :void :pointer)
  (("glWindowPos2iARB" glWindowPos2iARB) :void GLint GLint)
  (("glWindowPos2ivARB" glWindowPos2ivARB) :void :pointer)
  (("glWindowPos2sARB" glWindowPos2sARB) :void GLshort GLshort)
  (("glWindowPos2svARB" glWindowPos2svARB) :void :pointer)
  (("glWindowPos3dARB" glWindowPos3dARB) :void GLdouble GLdouble GLdouble)
  (("glWindowPos3dvARB" glWindowPos3dvARB) :void :pointer)
  (("glWindowPos3fARB" glWindowPos3fARB) :void GLfloat GLfloat GLfloat)
  (("glWindowPos3fvARB" glWindowPos3fvARB) :void :pointer)
  (("glWindowPos3iARB" glWindowPos3iARB) :void GLint GLint GLint)
  (("glWindowPos3ivARB" glWindowPos3ivARB) :void :pointer)
  (("glWindowPos3sARB" glWindowPos3sARB) :void GLshort GLshort GLshort)
  (("glWindowPos3svARB" glWindowPos3svARB) :void :pointer))
(define-extension :gl_arb_vertex_program
  (("glVertexAttrib1dARB" glVertexAttrib1dARB) :void GLuint GLdouble)
  (("glVertexAttrib1dvARB" glVertexAttrib1dvARB) :void GLuint :pointer)
  (("glVertexAttrib1fARB" glVertexAttrib1fARB) :void GLuint GLfloat)
  (("glVertexAttrib1fvARB" glVertexAttrib1fvARB) :void GLuint :pointer)
  (("glVertexAttrib1sARB" glVertexAttrib1sARB) :void GLuint GLshort)
  (("glVertexAttrib1svARB" glVertexAttrib1svARB) :void GLuint :pointer)
  (("glVertexAttrib2dARB" glVertexAttrib2dARB) :void GLuint GLdouble GLdouble)
  (("glVertexAttrib2dvARB" glVertexAttrib2dvARB) :void GLuint :pointer)
  (("glVertexAttrib2fARB" glVertexAttrib2fARB) :void GLuint GLfloat GLfloat)
  (("glVertexAttrib2fvARB" glVertexAttrib2fvARB) :void GLuint :pointer)
  (("glVertexAttrib2sARB" glVertexAttrib2sARB) :void GLuint GLshort GLshort)
  (("glVertexAttrib2svARB" glVertexAttrib2svARB) :void GLuint :pointer)
  (("glVertexAttrib3dARB" glVertexAttrib3dARB) :void GLuint GLdouble GLdouble
   GLdouble)
  (("glVertexAttrib3dvARB" glVertexAttrib3dvARB) :void GLuint :pointer)
  (("glVertexAttrib3fARB" glVertexAttrib3fARB) :void GLuint GLfloat GLfloat
   GLfloat)
  (("glVertexAttrib3fvARB" glVertexAttrib3fvARB) :void GLuint :pointer)
  (("glVertexAttrib3sARB" glVertexAttrib3sARB) :void GLuint GLshort GLshort
   GLshort)
  (("glVertexAttrib3svARB" glVertexAttrib3svARB) :void GLuint :pointer)
  (("glVertexAttrib4NbvARB" glVertexAttrib4NbvARB) :void GLuint :pointer)
  (("glVertexAttrib4NivARB" glVertexAttrib4NivARB) :void GLuint :pointer)
  (("glVertexAttrib4NsvARB" glVertexAttrib4NsvARB) :void GLuint :pointer)
  (("glVertexAttrib4NubARB" glVertexAttrib4NubARB) :void GLuint GLubyte GLubyte
   GLubyte GLubyte)
  (("glVertexAttrib4NubvARB" glVertexAttrib4NubvARB) :void GLuint :pointer)
  (("glVertexAttrib4NuivARB" glVertexAttrib4NuivARB) :void GLuint :pointer)
  (("glVertexAttrib4NusvARB" glVertexAttrib4NusvARB) :void GLuint :pointer)
  (("glVertexAttrib4bvARB" glVertexAttrib4bvARB) :void GLuint :pointer)
  (("glVertexAttrib4dARB" glVertexAttrib4dARB) :void GLuint GLdouble GLdouble
   GLdouble GLdouble)
  (("glVertexAttrib4dvARB" glVertexAttrib4dvARB) :void GLuint :pointer)
  (("glVertexAttrib4fARB" glVertexAttrib4fARB) :void GLuint GLfloat GLfloat
   GLfloat GLfloat)
  (("glVertexAttrib4fvARB" glVertexAttrib4fvARB) :void GLuint :pointer)
  (("glVertexAttrib4ivARB" glVertexAttrib4ivARB) :void GLuint :pointer)
  (("glVertexAttrib4sARB" glVertexAttrib4sARB) :void GLuint GLshort GLshort
   GLshort GLshort)
  (("glVertexAttrib4svARB" glVertexAttrib4svARB) :void GLuint :pointer)
  (("glVertexAttrib4ubvARB" glVertexAttrib4ubvARB) :void GLuint :pointer)
  (("glVertexAttrib4uivARB" glVertexAttrib4uivARB) :void GLuint :pointer)
  (("glVertexAttrib4usvARB" glVertexAttrib4usvARB) :void GLuint :pointer)
  (("glVertexAttribPointerARB" glVertexAttribPointerARB) :void GLuint GLint
   GLenum GLboolean GLsizei :pointer)
  (("glEnableVertexAttribArrayARB" glEnableVertexAttribArrayARB) :void GLuint)
  (("glDisableVertexAttribArrayARB" glDisableVertexAttribArrayARB) :void
   GLuint)
  (("glProgramStringARB" glProgramStringARB) :void GLenum GLenum GLsizei
   :pointer)
  (("glBindProgramARB" glBindProgramARB) :void GLenum GLuint)
  (("glDeleteProgramsARB" glDeleteProgramsARB) :void GLsizei :pointer)
  (("glGenProgramsARB" glGenProgramsARB) :void GLsizei :pointer)
  (("glProgramEnvParameter4dARB" glProgramEnvParameter4dARB) :void GLenum
   GLuint GLdouble GLdouble GLdouble GLdouble)
  (("glProgramEnvParameter4dvARB" glProgramEnvParameter4dvARB) :void GLenum
   GLuint :pointer)
  (("glProgramEnvParameter4fARB" glProgramEnvParameter4fARB) :void GLenum
   GLuint GLfloat GLfloat GLfloat GLfloat)
  (("glProgramEnvParameter4fvARB" glProgramEnvParameter4fvARB) :void GLenum
   GLuint :pointer)
  (("glProgramLocalParameter4dARB" glProgramLocalParameter4dARB) :void GLenum
   GLuint GLdouble GLdouble GLdouble GLdouble)
  (("glProgramLocalParameter4dvARB" glProgramLocalParameter4dvARB) :void GLenum
   GLuint :pointer)
  (("glProgramLocalParameter4fARB" glProgramLocalParameter4fARB) :void GLenum
   GLuint GLfloat GLfloat GLfloat GLfloat)
  (("glProgramLocalParameter4fvARB" glProgramLocalParameter4fvARB) :void GLenum
   GLuint :pointer)
  (("glGetProgramEnvParameterdvARB" glGetProgramEnvParameterdvARB) :void GLenum
   GLuint :pointer)
  (("glGetProgramEnvParameterfvARB" glGetProgramEnvParameterfvARB) :void GLenum
   GLuint :pointer)
  (("glGetProgramLocalParameterdvARB" glGetProgramLocalParameterdvARB) :void
   GLenum GLuint :pointer)
  (("glGetProgramLocalParameterfvARB" glGetProgramLocalParameterfvARB) :void
   GLenum GLuint :pointer)
  (("glGetProgramivARB" glGetProgramivARB) :void GLenum GLenum :pointer)
  (("glGetProgramStringARB" glGetProgramStringARB) :void GLenum GLenum
   :pointer)
  (("glGetVertexAttribdvARB" glGetVertexAttribdvARB) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribfvARB" glGetVertexAttribfvARB) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribivARB" glGetVertexAttribivARB) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribPointervARB" glGetVertexAttribPointervARB) :void GLuint
   GLenum :pointer)
  (("glIsProgramARB" glIsProgramARB) GLboolean GLuint))
(define-extension :gl_arb_fragment_program)
(define-extension :gl_arb_vertex_buffer_object
  (("glBindBufferARB" glBindBufferARB) :void GLenum GLuint)
  (("glDeleteBuffersARB" glDeleteBuffersARB) :void GLsizei :pointer)
  (("glGenBuffersARB" glGenBuffersARB) :void GLsizei :pointer)
  (("glIsBufferARB" glIsBufferARB) GLboolean GLuint)
  (("glBufferDataARB" glBufferDataARB) :void GLenum glsizeiptrarb :pointer
   GLenum)
  (("glBufferSubDataARB" glBufferSubDataARB) :void GLenum GLintptrARB
   glsizeiptrarb :pointer)
  (("glGetBufferSubDataARB" glGetBufferSubDataARB) :void GLenum GLintptrARB
   glsizeiptrarb :pointer)
  (("glMapBufferARB" glMapBufferARB) :pointer GLenum GLenum)
  (("glUnmapBufferARB" glUnmapBufferARB) GLboolean GLenum)
  (("glGetBufferParameterivARB" glGetBufferParameterivARB) :void GLenum GLenum
   :pointer)
  (("glGetBufferPointervARB" glGetBufferPointervARB) :void GLenum GLenum
   :pointer))
(define-extension :gl_arb_occlusion_query
  (("glGenQueriesARB" glGenQueriesARB) :void GLsizei :pointer)
  (("glDeleteQueriesARB" glDeleteQueriesARB) :void GLsizei :pointer)
  (("glIsQueryARB" glIsQueryARB) GLboolean GLuint)
  (("glBeginQueryARB" glBeginQueryARB) :void GLenum GLuint)
  (("glEndQueryARB" glEndQueryARB) :void GLenum)
  (("glGetQueryivARB" glGetQueryivARB) :void GLenum GLenum :pointer)
  (("glGetQueryObjectivARB" glGetQueryObjectivARB) :void GLuint GLenum
   :pointer)
  (("glGetQueryObjectuivARB" glGetQueryObjectuivARB) :void GLuint GLenum
   :pointer))
(define-extension :gl_arb_shader_objects
  (("glDeleteObjectARB" glDeleteObjectARB) :void GLhandleARB)
  (("glGetHandleARB" glGetHandleARB) GLhandleARB GLenum)
  (("glDetachObjectARB" glDetachObjectARB) :void GLhandleARB GLhandleARB)
  (("glCreateShaderObjectARB" glCreateShaderObjectARB) GLhandleARB GLenum)
  (("glShaderSourceARB" glShaderSourceARB) :void GLhandleARB GLsizei :pointer
   :pointer)
  (("glCompileShaderARB" glCompileShaderARB) :void GLhandleARB)
  (("glCreateProgramObjectARB" glCreateProgramObjectARB) GLhandleARB)
  (("glAttachObjectARB" glAttachObjectARB) :void GLhandleARB GLhandleARB)
  (("glLinkProgramARB" glLinkProgramARB) :void GLhandleARB)
  (("glUseProgramObjectARB" glUseProgramObjectARB) :void GLhandleARB)
  (("glValidateProgramARB" glValidateProgramARB) :void GLhandleARB)
  (("glUniform1fARB" glUniform1fARB) :void GLint GLfloat)
  (("glUniform2fARB" glUniform2fARB) :void GLint GLfloat GLfloat)
  (("glUniform3fARB" glUniform3fARB) :void GLint GLfloat GLfloat GLfloat)
  (("glUniform4fARB" glUniform4fARB) :void GLint GLfloat GLfloat GLfloat
   GLfloat)
  (("glUniform1iARB" glUniform1iARB) :void GLint GLint)
  (("glUniform2iARB" glUniform2iARB) :void GLint GLint GLint)
  (("glUniform3iARB" glUniform3iARB) :void GLint GLint GLint GLint)
  (("glUniform4iARB" glUniform4iARB) :void GLint GLint GLint GLint GLint)
  (("glUniform1fvARB" glUniform1fvARB) :void GLint GLsizei :pointer)
  (("glUniform2fvARB" glUniform2fvARB) :void GLint GLsizei :pointer)
  (("glUniform3fvARB" glUniform3fvARB) :void GLint GLsizei :pointer)
  (("glUniform4fvARB" glUniform4fvARB) :void GLint GLsizei :pointer)
  (("glUniform1ivARB" glUniform1ivARB) :void GLint GLsizei :pointer)
  (("glUniform2ivARB" glUniform2ivARB) :void GLint GLsizei :pointer)
  (("glUniform3ivARB" glUniform3ivARB) :void GLint GLsizei :pointer)
  (("glUniform4ivARB" glUniform4ivARB) :void GLint GLsizei :pointer)
  (("glUniformMatrix2fvARB" glUniformMatrix2fvARB) :void GLint GLsizei
   GLboolean :pointer)
  (("glUniformMatrix3fvARB" glUniformMatrix3fvARB) :void GLint GLsizei
   GLboolean :pointer)
  (("glUniformMatrix4fvARB" glUniformMatrix4fvARB) :void GLint GLsizei
   GLboolean :pointer)
  (("glGetObjectParameterfvARB" glGetObjectParameterfvARB) :void GLhandleARB
   GLenum :pointer)
  (("glGetObjectParameterivARB" glGetObjectParameterivARB) :void GLhandleARB
   GLenum :pointer)
  (("glGetInfoLogARB" glGetInfoLogARB) :void GLhandleARB GLsizei :pointer
   :pointer)
  (("glGetAttachedObjectsARB" glGetAttachedObjectsARB) :void GLhandleARB
   GLsizei :pointer :pointer)
  (("glGetUniformLocationARB" glGetUniformLocationARB) GLint GLhandleARB
   :pointer)
  (("glGetActiveUniformARB" glGetActiveUniformARB) :void GLhandleARB GLuint
   GLsizei :pointer :pointer :pointer :pointer)
  (("glGetUniformfvARB" glGetUniformfvARB) :void GLhandleARB GLint :pointer)
  (("glGetUniformivARB" glGetUniformivARB) :void GLhandleARB GLint :pointer)
  (("glGetShaderSourceARB" glGetShaderSourceARB) :void GLhandleARB GLsizei
   :pointer :pointer))
(define-extension :gl_arb_vertex_shader
  (("glBindAttribLocationARB" glBindAttribLocationARB) :void GLhandleARB GLuint
   :pointer)
  (("glGetActiveAttribARB" glGetActiveAttribARB) :void GLhandleARB GLuint
   GLsizei :pointer :pointer :pointer :pointer)
  (("glGetAttribLocationARB" glGetAttribLocationARB) GLint GLhandleARB
   :pointer))
(define-extension :gl_arb_fragment_shader)
(define-extension :gl_arb_shading_language_100)
(define-extension :gl_arb_texture_non_power_of_two)
(define-extension :gl_arb_point_sprite)
(define-extension :gl_arb_fragment_program_shadow)
(define-extension :gl_arb_draw_buffers
  (("glDrawBuffersARB" glDrawBuffersARB) :void GLsizei :pointer))
(define-extension :gl_arb_texture_rectangle)
(define-extension :gl_ext_abgr)
(define-extension :gl_ext_blend_color
  (("glBlendColorEXT" glBlendColorEXT) :void GLclampf GLclampf GLclampf
   GLclampf))
(define-extension :gl_ext_polygon_offset
  (("glPolygonOffsetEXT" glPolygonOffsetEXT) :void GLfloat GLfloat))
(define-extension :gl_ext_texture)
(define-extension :gl_ext_texture3d
  (("glTexImage3DEXT" glTexImage3DEXT) :void GLenum GLint GLenum GLsizei
   GLsizei GLsizei GLint GLenum GLenum :pointer)
  (("glTexSubImage3DEXT" glTexSubImage3DEXT) :void GLenum GLint GLint GLint
   GLint GLsizei GLsizei GLsizei GLenum GLenum :pointer))
(define-extension :gl_sgis_texture_filter4
  (("glGetTexFilterFuncSGIS" glGetTexFilterFuncSGIS) :void GLenum GLenum
   :pointer)
  (("glTexFilterFuncSGIS" glTexFilterFuncSGIS) :void GLenum GLenum GLsizei
   :pointer))
(define-extension :gl_ext_subtexture
  (("glTexSubImage1DEXT" glTexSubImage1DEXT) :void GLenum GLint GLint GLsizei
   GLenum GLenum :pointer)
  (("glTexSubImage2DEXT" glTexSubImage2DEXT) :void GLenum GLint GLint GLint
   GLsizei GLsizei GLenum GLenum :pointer))
(define-extension :gl_ext_copy_texture
  (("glCopyTexImage1DEXT" glCopyTexImage1DEXT) :void GLenum GLint GLenum GLint
   GLint GLsizei GLint)
  (("glCopyTexImage2DEXT" glCopyTexImage2DEXT) :void GLenum GLint GLenum GLint
   GLint GLsizei GLsizei GLint)
  (("glCopyTexSubImage1DEXT" glCopyTexSubImage1DEXT) :void GLenum GLint GLint
   GLint GLint GLsizei)
  (("glCopyTexSubImage2DEXT" glCopyTexSubImage2DEXT) :void GLenum GLint GLint
   GLint GLint GLint GLsizei GLsizei)
  (("glCopyTexSubImage3DEXT" glCopyTexSubImage3DEXT) :void GLenum GLint GLint
   GLint GLint GLint GLint GLsizei GLsizei))
(define-extension :gl_ext_histogram
  (("glGetHistogramEXT" glGetHistogramEXT) :void GLenum GLboolean GLenum GLenum
   :pointer)
  (("glGetHistogramParameterfvEXT" glGetHistogramParameterfvEXT) :void GLenum
   GLenum :pointer)
  (("glGetHistogramParameterivEXT" glGetHistogramParameterivEXT) :void GLenum
   GLenum :pointer)
  (("glGetMinmaxEXT" glGetMinmaxEXT) :void GLenum GLboolean GLenum GLenum
   :pointer)
  (("glGetMinmaxParameterfvEXT" glGetMinmaxParameterfvEXT) :void GLenum GLenum
   :pointer)
  (("glGetMinmaxParameterivEXT" glGetMinmaxParameterivEXT) :void GLenum GLenum
   :pointer)
  (("glHistogramEXT" glHistogramEXT) :void GLenum GLsizei GLenum GLboolean)
  (("glMinmaxEXT" glMinmaxEXT) :void GLenum GLenum GLboolean)
  (("glResetHistogramEXT" glResetHistogramEXT) :void GLenum)
  (("glResetMinmaxEXT" glResetMinmaxEXT) :void GLenum))
(define-extension :gl_ext_convolution
  (("glConvolutionFilter1DEXT" glConvolutionFilter1DEXT) :void GLenum GLenum
   GLsizei GLenum GLenum :pointer)
  (("glConvolutionFilter2DEXT" glConvolutionFilter2DEXT) :void GLenum GLenum
   GLsizei GLsizei GLenum GLenum :pointer)
  (("glConvolutionParameterfEXT" glConvolutionParameterfEXT) :void GLenum
   GLenum GLfloat)
  (("glConvolutionParameterfvEXT" glConvolutionParameterfvEXT) :void GLenum
   GLenum :pointer)
  (("glConvolutionParameteriEXT" glConvolutionParameteriEXT) :void GLenum
   GLenum GLint)
  (("glConvolutionParameterivEXT" glConvolutionParameterivEXT) :void GLenum
   GLenum :pointer)
  (("glCopyConvolutionFilter1DEXT" glCopyConvolutionFilter1DEXT) :void GLenum
   GLenum GLint GLint GLsizei)
  (("glCopyConvolutionFilter2DEXT" glCopyConvolutionFilter2DEXT) :void GLenum
   GLenum GLint GLint GLsizei GLsizei)
  (("glGetConvolutionFilterEXT" glGetConvolutionFilterEXT) :void GLenum GLenum
   GLenum :pointer)
  (("glGetConvolutionParameterfvEXT" glGetConvolutionParameterfvEXT) :void
   GLenum GLenum :pointer)
  (("glGetConvolutionParameterivEXT" glGetConvolutionParameterivEXT) :void
   GLenum GLenum :pointer)
  (("glGetSeparableFilterEXT" glGetSeparableFilterEXT) :void GLenum GLenum
   GLenum :pointer :pointer :pointer)
  (("glSeparableFilter2DEXT" glSeparableFilter2DEXT) :void GLenum GLenum
   GLsizei GLsizei GLenum GLenum :pointer :pointer))
(define-extension :gl_ext_color_matrix)
(define-extension :gl_sgi_color_table
  (("glColorTableSGI" glColorTableSGI) :void GLenum GLenum GLsizei GLenum
   GLenum :pointer)
  (("glColorTableParameterfvSGI" glColorTableParameterfvSGI) :void GLenum
   GLenum :pointer)
  (("glColorTableParameterivSGI" glColorTableParameterivSGI) :void GLenum
   GLenum :pointer)
  (("glCopyColorTableSGI" glCopyColorTableSGI) :void GLenum GLenum GLint GLint
   GLsizei)
  (("glGetColorTableSGI" glGetColorTableSGI) :void GLenum GLenum GLenum
   :pointer)
  (("glGetColorTableParameterfvSGI" glGetColorTableParameterfvSGI) :void GLenum
   GLenum :pointer)
  (("glGetColorTableParameterivSGI" glGetColorTableParameterivSGI) :void GLenum
   GLenum :pointer))
(define-extension :gl_sgix_pixel_texture
  (("glPixelTexGenSGIX" glPixelTexGenSGIX) :void GLenum))
(define-extension :gl_sgis_pixel_texture
  (("glPixelTexGenParameteriSGIS" glPixelTexGenParameteriSGIS) :void GLenum
   GLint)
  (("glPixelTexGenParameterivSGIS" glPixelTexGenParameterivSGIS) :void GLenum
   :pointer)
  (("glPixelTexGenParameterfSGIS" glPixelTexGenParameterfSGIS) :void GLenum
   GLfloat)
  (("glPixelTexGenParameterfvSGIS" glPixelTexGenParameterfvSGIS) :void GLenum
   :pointer)
  (("glGetPixelTexGenParameterivSGIS" glGetPixelTexGenParameterivSGIS) :void
   GLenum :pointer)
  (("glGetPixelTexGenParameterfvSGIS" glGetPixelTexGenParameterfvSGIS) :void
   GLenum :pointer))
(define-extension :gl_sgis_texture4d
  (("glTexImage4DSGIS" glTexImage4DSGIS) :void GLenum GLint GLenum GLsizei
   GLsizei GLsizei GLsizei GLint GLenum GLenum :pointer)
  (("glTexSubImage4DSGIS" glTexSubImage4DSGIS) :void GLenum GLint GLint GLint
   GLint GLint GLsizei GLsizei GLsizei GLsizei GLenum GLenum :pointer))
(define-extension :gl_sgi_texture_color_table)
(define-extension :gl_ext_cmyka)
(define-extension :gl_ext_texture_object
  (("glAreTexturesResidentEXT" glAreTexturesResidentEXT) GLboolean GLsizei
   :pointer :pointer)
  (("glBindTextureEXT" glBindTextureEXT) :void GLenum GLuint)
  (("glDeleteTexturesEXT" glDeleteTexturesEXT) :void GLsizei :pointer)
  (("glGenTexturesEXT" glGenTexturesEXT) :void GLsizei :pointer)
  (("glIsTextureEXT" glIsTextureEXT) GLboolean GLuint)
  (("glPrioritizeTexturesEXT" glPrioritizeTexturesEXT) :void GLsizei :pointer
   :pointer))
(define-extension :gl_sgis_detail_texture
  (("glDetailTexFuncSGIS" glDetailTexFuncSGIS) :void GLenum GLsizei :pointer)
  (("glGetDetailTexFuncSGIS" glGetDetailTexFuncSGIS) :void GLenum :pointer))
(define-extension :gl_sgis_sharpen_texture
  (("glSharpenTexFuncSGIS" glSharpenTexFuncSGIS) :void GLenum GLsizei :pointer)
  (("glGetSharpenTexFuncSGIS" glGetSharpenTexFuncSGIS) :void GLenum :pointer))
(define-extension :gl_ext_packed_pixels)
(define-extension :gl_sgis_texture_lod)
(define-extension :gl_sgis_multisample
  (("glSampleMaskSGIS" glSampleMaskSGIS) :void GLclampf GLboolean)
  (("glSamplePatternSGIS" glSamplePatternSGIS) :void GLenum))
(define-extension :gl_ext_rescale_normal)
(define-extension :gl_ext_vertex_array
  (("glArrayElementEXT" glArrayElementEXT) :void GLint)
  (("glColorPointerEXT" glColorPointerEXT) :void GLint GLenum GLsizei GLsizei
   :pointer)
  (("glDrawArraysEXT" glDrawArraysEXT) :void GLenum GLint GLsizei)
  (("glEdgeFlagPointerEXT" glEdgeFlagPointerEXT) :void GLsizei GLsizei
   :pointer)
  (("glGetPointervEXT" glGetPointervEXT) :void GLenum :pointer)
  (("glIndexPointerEXT" glIndexPointerEXT) :void GLenum GLsizei GLsizei
   :pointer)
  (("glNormalPointerEXT" glNormalPointerEXT) :void GLenum GLsizei GLsizei
   :pointer)
  (("glTexCoordPointerEXT" glTexCoordPointerEXT) :void GLint GLenum GLsizei
   GLsizei :pointer)
  (("glVertexPointerEXT" glVertexPointerEXT) :void GLint GLenum GLsizei GLsizei
   :pointer))
(define-extension :gl_ext_misc_attribute)
(define-extension :gl_sgis_generate_mipmap)
(define-extension :gl_sgix_clipmap)
(define-extension :gl_sgix_shadow)
(define-extension :gl_sgis_texture_edge_clamp)
(define-extension :gl_sgis_texture_border_clamp)
(define-extension :gl_ext_blend_minmax
  (("glBlendEquationEXT" glBlendEquationEXT) :void GLenum))
(define-extension :gl_ext_blend_subtract)
(define-extension :gl_ext_blend_logic_op)
(define-extension :gl_sgix_interlace)
(define-extension :gl_sgix_pixel_tiles)
(define-extension :gl_sgix_texture_select)
(define-extension :gl_sgix_sprite
  (("glSpriteParameterfSGIX" glSpriteParameterfSGIX) :void GLenum GLfloat)
  (("glSpriteParameterfvSGIX" glSpriteParameterfvSGIX) :void GLenum :pointer)
  (("glSpriteParameteriSGIX" glSpriteParameteriSGIX) :void GLenum GLint)
  (("glSpriteParameterivSGIX" glSpriteParameterivSGIX) :void GLenum :pointer))
(define-extension :gl_sgix_texture_multi_buffer)
(define-extension :gl_ext_point_parameters
  (("glPointParameterfEXT" glPointParameterfEXT) :void GLenum GLfloat)
  (("glPointParameterfvEXT" glPointParameterfvEXT) :void GLenum :pointer))
(define-extension :gl_sgis_point_parameters
  (("glPointParameterfSGIS" glPointParameterfSGIS) :void GLenum GLfloat)
  (("glPointParameterfvSGIS" glPointParameterfvSGIS) :void GLenum :pointer))
(define-extension :gl_sgix_instruments
  (("glGetInstrumentsSGIX" glGetInstrumentsSGIX) GLint)
  (("glInstrumentsBufferSGIX" glInstrumentsBufferSGIX) :void GLsizei :pointer)
  (("glPollInstrumentsSGIX" glPollInstrumentsSGIX) GLint :pointer)
  (("glReadInstrumentsSGIX" glReadInstrumentsSGIX) :void GLint)
  (("glStartInstrumentsSGIX" glStartInstrumentsSGIX) :void)
  (("glStopInstrumentsSGIX" glStopInstrumentsSGIX) :void GLint))
(define-extension :gl_sgix_texture_scale_bias)
(define-extension :gl_sgix_framezoom
  (("glFrameZoomSGIX" glFrameZoomSGIX) :void GLint))
(define-extension :gl_sgix_tag_sample_buffer
  (("glTagSampleBufferSGIX" glTagSampleBufferSGIX) :void))
(define-extension :gl_sgix_polynomial_ffd
  (("glDeformationMap3dSGIX" glDeformationMap3dSGIX) :void GLenum GLdouble
   GLdouble GLint GLint GLdouble GLdouble GLint GLint GLdouble GLdouble GLint
   GLint :pointer)
  (("glDeformationMap3fSGIX" glDeformationMap3fSGIX) :void GLenum GLfloat
   GLfloat GLint GLint GLfloat GLfloat GLint GLint GLfloat GLfloat GLint GLint
   :pointer)
  (("glDeformSGIX" glDeformSGIX) :void GLbitfield)
  (("glLoadIdentityDeformationMapSGIX" glLoadIdentityDeformationMapSGIX) :void
   GLbitfield))
(define-extension :gl_sgix_reference_plane
  (("glReferencePlaneSGIX" glReferencePlaneSGIX) :void :pointer))
(define-extension :gl_sgix_flush_raster
  (("glFlushRasterSGIX" glFlushRasterSGIX) :void))
(define-extension :gl_sgix_depth_texture)
(define-extension :gl_sgis_fog_function
  (("glFogFuncSGIS" glFogFuncSGIS) :void GLsizei :pointer)
  (("glGetFogFuncSGIS" glGetFogFuncSGIS) :void :pointer))
(define-extension :gl_sgix_fog_offset)
(define-extension :gl_hp_image_transform
  (("glImageTransformParameteriHP" glImageTransformParameteriHP) :void GLenum
   GLenum GLint)
  (("glImageTransformParameterfHP" glImageTransformParameterfHP) :void GLenum
   GLenum GLfloat)
  (("glImageTransformParameterivHP" glImageTransformParameterivHP) :void GLenum
   GLenum :pointer)
  (("glImageTransformParameterfvHP" glImageTransformParameterfvHP) :void GLenum
   GLenum :pointer)
  (("glGetImageTransformParameterivHP" glGetImageTransformParameterivHP) :void
   GLenum GLenum :pointer)
  (("glGetImageTransformParameterfvHP" glGetImageTransformParameterfvHP) :void
   GLenum GLenum :pointer))
(define-extension :gl_hp_convolution_border_modes)
(define-extension :gl_sgix_texture_add_env)
(define-extension :gl_ext_color_subtable
  (("glColorSubTableEXT" glColorSubTableEXT) :void GLenum GLsizei GLsizei
   GLenum GLenum :pointer)
  (("glCopyColorSubTableEXT" glCopyColorSubTableEXT) :void GLenum GLsizei GLint
   GLint GLsizei))
(define-extension :gl_pgi_vertex_hints)
(define-extension :gl_pgi_misc_hints
  (("glHintPGI" glHintPGI) :void GLenum GLint))
(define-extension :gl_ext_paletted_texture
  (("glColorTableEXT" glColorTableEXT) :void GLenum GLenum GLsizei GLenum
   GLenum :pointer)
  (("glGetColorTableEXT" glGetColorTableEXT) :void GLenum GLenum GLenum
   :pointer)
  (("glGetColorTableParameterivEXT" glGetColorTableParameterivEXT) :void GLenum
   GLenum :pointer)
  (("glGetColorTableParameterfvEXT" glGetColorTableParameterfvEXT) :void GLenum
   GLenum :pointer))
(define-extension :gl_ext_clip_volume_hint)
(define-extension :gl_sgix_list_priority
  (("glGetListParameterfvSGIX" glGetListParameterfvSGIX) :void GLuint GLenum
   :pointer)
  (("glGetListParameterivSGIX" glGetListParameterivSGIX) :void GLuint GLenum
   :pointer)
  (("glListParameterfSGIX" glListParameterfSGIX) :void GLuint GLenum GLfloat)
  (("glListParameterfvSGIX" glListParameterfvSGIX) :void GLuint GLenum
   :pointer)
  (("glListParameteriSGIX" glListParameteriSGIX) :void GLuint GLenum GLint)
  (("glListParameterivSGIX" glListParameterivSGIX) :void GLuint GLenum
   :pointer))
(define-extension :gl_sgix_ir_instrument1)
(define-extension :gl_sgix_calligraphic_fragment)
(define-extension :gl_sgix_texture_lod_bias)
(define-extension :gl_sgix_shadow_ambient)
(define-extension :gl_ext_index_texture)
(define-extension :gl_ext_index_material
  (("glIndexMaterialEXT" glIndexMaterialEXT) :void GLenum GLenum))
(define-extension :gl_ext_index_func
  (("glIndexFuncEXT" glIndexFuncEXT) :void GLenum GLclampf))
(define-extension :gl_ext_index_array_formats)
(define-extension :gl_ext_compiled_vertex_array
  (("glLockArraysEXT" glLockArraysEXT) :void GLint GLsizei)
  (("glUnlockArraysEXT" glUnlockArraysEXT) :void))
(define-extension :gl_ext_cull_vertex
  (("glCullParameterdvEXT" glCullParameterdvEXT) :void GLenum :pointer)
  (("glCullParameterfvEXT" glCullParameterfvEXT) :void GLenum :pointer))
(define-extension :gl_sgix_ycrcb)
(define-extension :gl_sgix_fragment_lighting
  (("glFragmentColorMaterialSGIX" glFragmentColorMaterialSGIX) :void GLenum
   GLenum)
  (("glFragmentLightfSGIX" glFragmentLightfSGIX) :void GLenum GLenum GLfloat)
  (("glFragmentLightfvSGIX" glFragmentLightfvSGIX) :void GLenum GLenum
   :pointer)
  (("glFragmentLightiSGIX" glFragmentLightiSGIX) :void GLenum GLenum GLint)
  (("glFragmentLightivSGIX" glFragmentLightivSGIX) :void GLenum GLenum
   :pointer)
  (("glFragmentLightModelfSGIX" glFragmentLightModelfSGIX) :void GLenum
   GLfloat)
  (("glFragmentLightModelfvSGIX" glFragmentLightModelfvSGIX) :void GLenum
   :pointer)
  (("glFragmentLightModeliSGIX" glFragmentLightModeliSGIX) :void GLenum GLint)
  (("glFragmentLightModelivSGIX" glFragmentLightModelivSGIX) :void GLenum
   :pointer)
  (("glFragmentMaterialfSGIX" glFragmentMaterialfSGIX) :void GLenum GLenum
   GLfloat)
  (("glFragmentMaterialfvSGIX" glFragmentMaterialfvSGIX) :void GLenum GLenum
   :pointer)
  (("glFragmentMaterialiSGIX" glFragmentMaterialiSGIX) :void GLenum GLenum
   GLint)
  (("glFragmentMaterialivSGIX" glFragmentMaterialivSGIX) :void GLenum GLenum
   :pointer)
  (("glGetFragmentLightfvSGIX" glGetFragmentLightfvSGIX) :void GLenum GLenum
   :pointer)
  (("glGetFragmentLightivSGIX" glGetFragmentLightivSGIX) :void GLenum GLenum
   :pointer)
  (("glGetFragmentMaterialfvSGIX" glGetFragmentMaterialfvSGIX) :void GLenum
   GLenum :pointer)
  (("glGetFragmentMaterialivSGIX" glGetFragmentMaterialivSGIX) :void GLenum
   GLenum :pointer)
  (("glLightEnviSGIX" glLightEnviSGIX) :void GLenum GLint))
(define-extension :gl_ibm_rasterpos_clip)
(define-extension :gl_hp_texture_lighting)
(define-extension :gl_ext_draw_range_elements
  (("glDrawRangeElementsEXT" glDrawRangeElementsEXT) :void GLenum GLuint GLuint
   GLsizei GLenum :pointer))
(define-extension :gl_win_phong_shading)
(define-extension :gl_win_specular_fog)
(define-extension :gl_ext_light_texture
  (("glApplyTextureEXT" glApplyTextureEXT) :void GLenum)
  (("glTextureLightEXT" glTextureLightEXT) :void GLenum)
  (("glTextureMaterialEXT" glTextureMaterialEXT) :void GLenum GLenum))
(define-extension :gl_sgix_blend_alpha_minmax)
(define-extension :gl_ext_bgra)
(define-extension :gl_sgix_async
  (("glAsyncMarkerSGIX" glAsyncMarkerSGIX) :void GLuint)
  (("glFinishAsyncSGIX" glFinishAsyncSGIX) GLint :pointer)
  (("glPollAsyncSGIX" glPollAsyncSGIX) GLint :pointer)
  (("glGenAsyncMarkersSGIX" glGenAsyncMarkersSGIX) GLuint GLsizei)
  (("glDeleteAsyncMarkersSGIX" glDeleteAsyncMarkersSGIX) :void GLuint GLsizei)
  (("glIsAsyncMarkerSGIX" glIsAsyncMarkerSGIX) GLboolean GLuint))
(define-extension :gl_sgix_async_pixel)
(define-extension :gl_sgix_async_histogram)
(define-extension :gl_intel_parallel_arrays
  (("glVertexPointervINTEL" glVertexPointervINTEL) :void GLint GLenum :pointer)
  (("glNormalPointervINTEL" glNormalPointervINTEL) :void GLenum :pointer)
  (("glColorPointervINTEL" glColorPointervINTEL) :void GLint GLenum :pointer)
  (("glTexCoordPointervINTEL" glTexCoordPointervINTEL) :void GLint GLenum
   :pointer))
(define-extension :gl_hp_occlusion_test)
(define-extension :gl_ext_pixel_transform
  (("glPixelTransformParameteriEXT" glPixelTransformParameteriEXT) :void GLenum
   GLenum GLint)
  (("glPixelTransformParameterfEXT" glPixelTransformParameterfEXT) :void GLenum
   GLenum GLfloat)
  (("glPixelTransformParameterivEXT" glPixelTransformParameterivEXT) :void
   GLenum GLenum :pointer)
  (("glPixelTransformParameterfvEXT" glPixelTransformParameterfvEXT) :void
   GLenum GLenum :pointer))
(define-extension :gl_ext_pixel_transform_color_table)
(define-extension :gl_ext_shared_texture_palette)
(define-extension :gl_ext_separate_specular_color)
(define-extension :gl_ext_secondary_color
  (("glSecondaryColor3bEXT" glSecondaryColor3bEXT) :void GLbyte GLbyte GLbyte)
  (("glSecondaryColor3bvEXT" glSecondaryColor3bvEXT) :void :pointer)
  (("glSecondaryColor3dEXT" glSecondaryColor3dEXT) :void GLdouble GLdouble
   GLdouble)
  (("glSecondaryColor3dvEXT" glSecondaryColor3dvEXT) :void :pointer)
  (("glSecondaryColor3fEXT" glSecondaryColor3fEXT) :void GLfloat GLfloat
   GLfloat)
  (("glSecondaryColor3fvEXT" glSecondaryColor3fvEXT) :void :pointer)
  (("glSecondaryColor3iEXT" glSecondaryColor3iEXT) :void GLint GLint GLint)
  (("glSecondaryColor3ivEXT" glSecondaryColor3ivEXT) :void :pointer)
  (("glSecondaryColor3sEXT" glSecondaryColor3sEXT) :void GLshort GLshort
   GLshort)
  (("glSecondaryColor3svEXT" glSecondaryColor3svEXT) :void :pointer)
  (("glSecondaryColor3ubEXT" glSecondaryColor3ubEXT) :void GLubyte GLubyte
   GLubyte)
  (("glSecondaryColor3ubvEXT" glSecondaryColor3ubvEXT) :void :pointer)
  (("glSecondaryColor3uiEXT" glSecondaryColor3uiEXT) :void GLuint GLuint
   GLuint)
  (("glSecondaryColor3uivEXT" glSecondaryColor3uivEXT) :void :pointer)
  (("glSecondaryColor3usEXT" glSecondaryColor3usEXT) :void GLushort GLushort
   GLushort)
  (("glSecondaryColor3usvEXT" glSecondaryColor3usvEXT) :void :pointer)
  (("glSecondaryColorPointerEXT" glSecondaryColorPointerEXT) :void GLint GLenum
   GLsizei :pointer))
(define-extension :gl_ext_texture_perturb_normal
  (("glTextureNormalEXT" glTextureNormalEXT) :void GLenum))
(define-extension :gl_ext_multi_draw_arrays
  (("glMultiDrawArraysEXT" glMultiDrawArraysEXT) :void GLenum :pointer :pointer
   GLsizei)
  (("glMultiDrawElementsEXT" glMultiDrawElementsEXT) :void GLenum :pointer
   GLenum :pointer GLsizei))
(define-extension :gl_ext_fog_coord
  (("glFogCoordfEXT" glFogCoordfEXT) :void GLfloat)
  (("glFogCoordfvEXT" glFogCoordfvEXT) :void :pointer)
  (("glFogCoorddEXT" glFogCoorddEXT) :void GLdouble)
  (("glFogCoorddvEXT" glFogCoorddvEXT) :void :pointer)
  (("glFogCoordPointerEXT" glFogCoordPointerEXT) :void GLenum GLsizei :pointer))
(define-extension :gl_rend_screen_coordinates)
(define-extension :gl_ext_coordinate_frame
  (("glTangent3bEXT" glTangent3bEXT) :void GLbyte GLbyte GLbyte)
  (("glTangent3bvEXT" glTangent3bvEXT) :void :pointer)
  (("glTangent3dEXT" glTangent3dEXT) :void GLdouble GLdouble GLdouble)
  (("glTangent3dvEXT" glTangent3dvEXT) :void :pointer)
  (("glTangent3fEXT" glTangent3fEXT) :void GLfloat GLfloat GLfloat)
  (("glTangent3fvEXT" glTangent3fvEXT) :void :pointer)
  (("glTangent3iEXT" glTangent3iEXT) :void GLint GLint GLint)
  (("glTangent3ivEXT" glTangent3ivEXT) :void :pointer)
  (("glTangent3sEXT" glTangent3sEXT) :void GLshort GLshort GLshort)
  (("glTangent3svEXT" glTangent3svEXT) :void :pointer)
  (("glBinormal3bEXT" glBinormal3bEXT) :void GLbyte GLbyte GLbyte)
  (("glBinormal3bvEXT" glBinormal3bvEXT) :void :pointer)
  (("glBinormal3dEXT" glBinormal3dEXT) :void GLdouble GLdouble GLdouble)
  (("glBinormal3dvEXT" glBinormal3dvEXT) :void :pointer)
  (("glBinormal3fEXT" glBinormal3fEXT) :void GLfloat GLfloat GLfloat)
  (("glBinormal3fvEXT" glBinormal3fvEXT) :void :pointer)
  (("glBinormal3iEXT" glBinormal3iEXT) :void GLint GLint GLint)
  (("glBinormal3ivEXT" glBinormal3ivEXT) :void :pointer)
  (("glBinormal3sEXT" glBinormal3sEXT) :void GLshort GLshort GLshort)
  (("glBinormal3svEXT" glBinormal3svEXT) :void :pointer)
  (("glTangentPointerEXT" glTangentPointerEXT) :void GLenum GLsizei :pointer)
  (("glBinormalPointerEXT" glBinormalPointerEXT) :void GLenum GLsizei :pointer))
(define-extension :gl_ext_texture_env_combine)
(define-extension :gl_apple_specular_vector)
(define-extension :gl_apple_transform_hint)
(define-extension :gl_sgix_fog_scale)
(define-extension :gl_sunx_constant_data
  (("glFinishTextureSUNX" glFinishTextureSUNX) :void))
(define-extension :gl_sun_global_alpha
  (("glGlobalAlphaFactorbSUN" glGlobalAlphaFactorbSUN) :void GLbyte)
  (("glGlobalAlphaFactorsSUN" glGlobalAlphaFactorsSUN) :void GLshort)
  (("glGlobalAlphaFactoriSUN" glGlobalAlphaFactoriSUN) :void GLint)
  (("glGlobalAlphaFactorfSUN" glGlobalAlphaFactorfSUN) :void GLfloat)
  (("glGlobalAlphaFactordSUN" glGlobalAlphaFactordSUN) :void GLdouble)
  (("glGlobalAlphaFactorubSUN" glGlobalAlphaFactorubSUN) :void GLubyte)
  (("glGlobalAlphaFactorusSUN" glGlobalAlphaFactorusSUN) :void GLushort)
  (("glGlobalAlphaFactoruiSUN" glGlobalAlphaFactoruiSUN) :void GLuint))
(define-extension :gl_sun_triangle_list
  (("glReplacementCodeuiSUN" glReplacementCodeuiSUN) :void GLuint)
  (("glReplacementCodeusSUN" glReplacementCodeusSUN) :void GLushort)
  (("glReplacementCodeubSUN" glReplacementCodeubSUN) :void GLubyte)
  (("glReplacementCodeuivSUN" glReplacementCodeuivSUN) :void :pointer)
  (("glReplacementCodeusvSUN" glReplacementCodeusvSUN) :void :pointer)
  (("glReplacementCodeubvSUN" glReplacementCodeubvSUN) :void :pointer)
  (("glReplacementCodePointerSUN" glReplacementCodePointerSUN) :void GLenum
   GLsizei :pointer))
(define-extension :gl_sun_vertex
  (("glColor4ubVertex2fSUN" glColor4ubVertex2fSUN) :void GLubyte GLubyte
   GLubyte GLubyte GLfloat GLfloat)
  (("glColor4ubVertex2fvSUN" glColor4ubVertex2fvSUN) :void :pointer :pointer)
  (("glColor4ubVertex3fSUN" glColor4ubVertex3fSUN) :void GLubyte GLubyte
   GLubyte GLubyte GLfloat GLfloat GLfloat)
  (("glColor4ubVertex3fvSUN" glColor4ubVertex3fvSUN) :void :pointer :pointer)
  (("glColor3fVertex3fSUN" glColor3fVertex3fSUN) :void GLfloat GLfloat GLfloat
   GLfloat GLfloat GLfloat)
  (("glColor3fVertex3fvSUN" glColor3fVertex3fvSUN) :void :pointer :pointer)
  (("glNormal3fVertex3fSUN" glNormal3fVertex3fSUN) :void GLfloat GLfloat
   GLfloat GLfloat GLfloat GLfloat)
  (("glNormal3fVertex3fvSUN" glNormal3fVertex3fvSUN) :void :pointer :pointer)
  (("glColor4fNormal3fVertex3fSUN" glColor4fNormal3fVertex3fSUN) :void GLfloat
   GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glColor4fNormal3fVertex3fvSUN" glColor4fNormal3fVertex3fvSUN) :void
   :pointer :pointer :pointer)
  (("glTexCoord2fVertex3fSUN" glTexCoord2fVertex3fSUN) :void GLfloat GLfloat
   GLfloat GLfloat GLfloat)
  (("glTexCoord2fVertex3fvSUN" glTexCoord2fVertex3fvSUN) :void :pointer
   :pointer)
  (("glTexCoord4fVertex4fSUN" glTexCoord4fVertex4fSUN) :void GLfloat GLfloat
   GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glTexCoord4fVertex4fvSUN" glTexCoord4fVertex4fvSUN) :void :pointer
   :pointer)
  (("glTexCoord2fColor4ubVertex3fSUN" glTexCoord2fColor4ubVertex3fSUN) :void
   GLfloat GLfloat GLubyte GLubyte GLubyte GLubyte GLfloat GLfloat GLfloat)
  (("glTexCoord2fColor4ubVertex3fvSUN" glTexCoord2fColor4ubVertex3fvSUN) :void
   :pointer :pointer :pointer)
  (("glTexCoord2fColor3fVertex3fSUN" glTexCoord2fColor3fVertex3fSUN) :void
   GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glTexCoord2fColor3fVertex3fvSUN" glTexCoord2fColor3fVertex3fvSUN) :void
   :pointer :pointer :pointer)
  (("glTexCoord2fNormal3fVertex3fSUN" glTexCoord2fNormal3fVertex3fSUN) :void
   GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glTexCoord2fNormal3fVertex3fvSUN" glTexCoord2fNormal3fVertex3fvSUN) :void
   :pointer :pointer :pointer)
  (("glTexCoord2fColor4fNormal3fVertex3fSUN"
    gltexcoord2fcolor4fnormal3fvertex3fsun)
   :void GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat
   GLfloat GLfloat GLfloat GLfloat)
  (("glTexCoord2fColor4fNormal3fVertex3fvSUN"
    gltexcoord2fcolor4fnormal3fvertex3fvsun)
   :void :pointer :pointer :pointer :pointer)
  (("glTexCoord4fColor4fNormal3fVertex4fSUN"
    gltexcoord4fcolor4fnormal3fvertex4fsun)
   :void GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat
   GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glTexCoord4fColor4fNormal3fVertex4fvSUN"
    gltexcoord4fcolor4fnormal3fvertex4fvsun)
   :void :pointer :pointer :pointer :pointer)
  (("glReplacementCodeuiVertex3fSUN" glReplacementCodeuiVertex3fSUN) :void
   GLuint GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiVertex3fvSUN" glReplacementCodeuiVertex3fvSUN) :void
   :pointer :pointer)
  (("glReplacementCodeuiColor4ubVertex3fSUN"
    glreplacementcodeuicolor4ubvertex3fsun)
   :void GLuint GLubyte GLubyte GLubyte GLubyte GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiColor4ubVertex3fvSUN"
    glreplacementcodeuicolor4ubvertex3fvsun)
   :void :pointer :pointer :pointer)
  (("glReplacementCodeuiColor3fVertex3fSUN"
    glreplacementcodeuicolor3fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiColor3fVertex3fvSUN"
    glreplacementcodeuicolor3fvertex3fvsun)
   :void :pointer :pointer :pointer)
  (("glReplacementCodeuiNormal3fVertex3fSUN"
    glreplacementcodeuinormal3fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiNormal3fVertex3fvSUN"
    glreplacementcodeuinormal3fvertex3fvsun)
   :void :pointer :pointer :pointer)
  (("glReplacementCodeuiColor4fNormal3fVertex3fSUN"
    glreplacementcodeuicolor4fnormal3fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat
   GLfloat GLfloat)
  (("glReplacementCodeuiColor4fNormal3fVertex3fvSUN"
    glreplacementcodeuicolor4fnormal3fvertex3fvsun)
   :void :pointer :pointer :pointer :pointer)
  (("glReplacementCodeuiTexCoord2fVertex3fSUN"
    glreplacementcodeuitexcoord2fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiTexCoord2fVertex3fvSUN"
    glreplacementcodeuitexcoord2fvertex3fvsun)
   :void :pointer :pointer :pointer)
  (("glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN"
    glreplacementcodeuitexcoord2fnormal3fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat
   GLfloat)
  (("glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN"
    glreplacementcodeuitexcoord2fnormal3fvertex3fvsun)
   :void :pointer :pointer :pointer :pointer)
  (("glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN"
    glreplacementcodeuitexcoord2fcolor4fnormal3fvertex3fsun)
   :void GLuint GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat GLfloat
   GLfloat GLfloat GLfloat GLfloat)
  (("glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN"
    glreplacementcodeuitexcoord2fcolor4fnormal3fvertex3fvsun)
   :void :pointer :pointer :pointer :pointer :pointer))
(define-extension :gl_ext_blend_func_separate
  (("glBlendFuncSeparateEXT" glBlendFuncSeparateEXT) :void GLenum GLenum GLenum
   GLenum))
(define-extension :gl_ingr_blend_func_separate
  (("glBlendFuncSeparateINGR" glBlendFuncSeparateINGR) :void GLenum GLenum
   GLenum GLenum))
(define-extension :gl_ingr_color_clamp)
(define-extension :gl_ingr_interlace_read)
(define-extension :gl_ext_stencil_wrap)
(define-extension :gl_ext_422_pixels)
(define-extension :gl_nv_texgen_reflection)
(define-extension :gl_sun_convolution_border_modes)
(define-extension :gl_ext_texture_env_add)
(define-extension :gl_ext_texture_lod_bias)
(define-extension :gl_ext_texture_filter_anisotropic)
(define-extension :gl_ext_vertex_weighting
  (("glVertexWeightfEXT" glVertexWeightfEXT) :void GLfloat)
  (("glVertexWeightfvEXT" glVertexWeightfvEXT) :void :pointer)
  (("glVertexWeightPointerEXT" glVertexWeightPointerEXT) :void GLsizei GLenum
   GLsizei :pointer))
(define-extension :gl_nv_light_max_exponent)
(define-extension :gl_nv_vertex_array_range
  (("glFlushVertexArrayRangeNV" glFlushVertexArrayRangeNV) :void)
  (("glVertexArrayRangeNV" glVertexArrayRangeNV) :void GLsizei :pointer))
(define-extension :gl_nv_register_combiners
  (("glCombinerParameterfvNV" glCombinerParameterfvNV) :void GLenum :pointer)
  (("glCombinerParameterfNV" glCombinerParameterfNV) :void GLenum GLfloat)
  (("glCombinerParameterivNV" glCombinerParameterivNV) :void GLenum :pointer)
  (("glCombinerParameteriNV" glCombinerParameteriNV) :void GLenum GLint)
  (("glCombinerInputNV" glCombinerInputNV) :void GLenum GLenum GLenum GLenum
   GLenum GLenum)
  (("glCombinerOutputNV" glCombinerOutputNV) :void GLenum GLenum GLenum GLenum
   GLenum GLenum GLenum GLboolean GLboolean GLboolean)
  (("glFinalCombinerInputNV" glFinalCombinerInputNV) :void GLenum GLenum GLenum
   GLenum)
  (("glGetCombinerInputParameterfvNV" glGetCombinerInputParameterfvNV) :void
   GLenum GLenum GLenum GLenum :pointer)
  (("glGetCombinerInputParameterivNV" glGetCombinerInputParameterivNV) :void
   GLenum GLenum GLenum GLenum :pointer)
  (("glGetCombinerOutputParameterfvNV" glGetCombinerOutputParameterfvNV) :void
   GLenum GLenum GLenum :pointer)
  (("glGetCombinerOutputParameterivNV" glGetCombinerOutputParameterivNV) :void
   GLenum GLenum GLenum :pointer)
  (("glGetFinalCombinerInputParameterfvNV"
    glgetfinalcombinerinputparameterfvnv)
   :void GLenum GLenum :pointer)
  (("glGetFinalCombinerInputParameterivNV"
    glgetfinalcombinerinputparameterivnv)
   :void GLenum GLenum :pointer))
(define-extension :gl_nv_fog_distance)
(define-extension :gl_nv_texgen_emboss)
(define-extension :gl_nv_blend_square)
(define-extension :gl_nv_texture_env_combine4)
(define-extension :gl_mesa_resize_buffers
  (("glResizeBuffersMESA" glResizeBuffersMESA) :void))
(define-extension :gl_mesa_window_pos
  (("glWindowPos2dMESA" glWindowPos2dMESA) :void GLdouble GLdouble)
  (("glWindowPos2dvMESA" glWindowPos2dvMESA) :void :pointer)
  (("glWindowPos2fMESA" glWindowPos2fMESA) :void GLfloat GLfloat)
  (("glWindowPos2fvMESA" glWindowPos2fvMESA) :void :pointer)
  (("glWindowPos2iMESA" glWindowPos2iMESA) :void GLint GLint)
  (("glWindowPos2ivMESA" glWindowPos2ivMESA) :void :pointer)
  (("glWindowPos2sMESA" glWindowPos2sMESA) :void GLshort GLshort)
  (("glWindowPos2svMESA" glWindowPos2svMESA) :void :pointer)
  (("glWindowPos3dMESA" glWindowPos3dMESA) :void GLdouble GLdouble GLdouble)
  (("glWindowPos3dvMESA" glWindowPos3dvMESA) :void :pointer)
  (("glWindowPos3fMESA" glWindowPos3fMESA) :void GLfloat GLfloat GLfloat)
  (("glWindowPos3fvMESA" glWindowPos3fvMESA) :void :pointer)
  (("glWindowPos3iMESA" glWindowPos3iMESA) :void GLint GLint GLint)
  (("glWindowPos3ivMESA" glWindowPos3ivMESA) :void :pointer)
  (("glWindowPos3sMESA" glWindowPos3sMESA) :void GLshort GLshort GLshort)
  (("glWindowPos3svMESA" glWindowPos3svMESA) :void :pointer)
  (("glWindowPos4dMESA" glWindowPos4dMESA) :void GLdouble GLdouble GLdouble
   GLdouble)
  (("glWindowPos4dvMESA" glWindowPos4dvMESA) :void :pointer)
  (("glWindowPos4fMESA" glWindowPos4fMESA) :void GLfloat GLfloat GLfloat
   GLfloat)
  (("glWindowPos4fvMESA" glWindowPos4fvMESA) :void :pointer)
  (("glWindowPos4iMESA" glWindowPos4iMESA) :void GLint GLint GLint GLint)
  (("glWindowPos4ivMESA" glWindowPos4ivMESA) :void :pointer)
  (("glWindowPos4sMESA" glWindowPos4sMESA) :void GLshort GLshort GLshort
   GLshort)
  (("glWindowPos4svMESA" glWindowPos4svMESA) :void :pointer))
(define-extension :gl_ibm_cull_vertex)
(define-extension :gl_ibm_multimode_draw_arrays
  (("glMultiModeDrawArraysIBM" glMultiModeDrawArraysIBM) :void :pointer
   :pointer :pointer GLsizei GLint)
  (("glMultiModeDrawElementsIBM" glMultiModeDrawElementsIBM) :void :pointer
   :pointer GLenum :pointer GLsizei GLint))
(define-extension :gl_ibm_vertex_array_lists
  (("glColorPointerListIBM" glColorPointerListIBM) :void GLint GLenum GLint
   :pointer GLint)
  (("glSecondaryColorPointerListIBM" glSecondaryColorPointerListIBM) :void
   GLint GLenum GLint :pointer GLint)
  (("glEdgeFlagPointerListIBM" glEdgeFlagPointerListIBM) :void GLint :pointer
   GLint)
  (("glFogCoordPointerListIBM" glFogCoordPointerListIBM) :void GLenum GLint
   :pointer GLint)
  (("glIndexPointerListIBM" glIndexPointerListIBM) :void GLenum GLint :pointer
   GLint)
  (("glNormalPointerListIBM" glNormalPointerListIBM) :void GLenum GLint
   :pointer GLint)
  (("glTexCoordPointerListIBM" glTexCoordPointerListIBM) :void GLint GLenum
   GLint :pointer GLint)
  (("glVertexPointerListIBM" glVertexPointerListIBM) :void GLint GLenum GLint
   :pointer GLint))
(define-extension :gl_sgix_subsample)
(define-extension :gl_sgix_ycrcba)
(define-extension :gl_sgix_ycrcb_subsample)
(define-extension :gl_sgix_depth_pass_instrument)
(define-extension :gl_3dfx_texture_compression_fxt1)
(define-extension :gl_3dfx_multisample)
(define-extension :gl_3dfx_tbuffer
  (("glTbufferMask3DFX" glTbufferMask3DFX) :void GLuint))
(define-extension :gl_ext_multisample
  (("glSampleMaskEXT" glSampleMaskEXT) :void GLclampf GLboolean)
  (("glSamplePatternEXT" glSamplePatternEXT) :void GLenum))
(define-extension :gl_sgix_vertex_preclip)
(define-extension :gl_sgix_convolution_accuracy)
(define-extension :gl_sgix_resample)
(define-extension :gl_sgis_point_line_texgen)
(define-extension :gl_sgis_texture_color_mask
  (("glTextureColorMaskSGIS" glTextureColorMaskSGIS) :void GLboolean GLboolean
   GLboolean GLboolean))
(define-extension :gl_sgix_igloo_interface
  (("glIglooInterfaceSGIX" glIglooInterfaceSGIX) :void GLenum :pointer))
(define-extension :gl_ext_texture_env_dot3)
(define-extension :gl_ati_texture_mirror_once)
(define-extension :gl_nv_fence
  (("glDeleteFencesNV" glDeleteFencesNV) :void GLsizei :pointer)
  (("glGenFencesNV" glGenFencesNV) :void GLsizei :pointer)
  (("glIsFenceNV" glIsFenceNV) GLboolean GLuint)
  (("glTestFenceNV" glTestFenceNV) GLboolean GLuint)
  (("glGetFenceivNV" glGetFenceivNV) :void GLuint GLenum :pointer)
  (("glFinishFenceNV" glFinishFenceNV) :void GLuint)
  (("glSetFenceNV" glSetFenceNV) :void GLuint GLenum))
(define-extension :gl_nv_evaluators
  (("glMapControlPointsNV" glMapControlPointsNV) :void GLenum GLuint GLenum
   GLsizei GLsizei GLint GLint GLboolean :pointer)
  (("glMapParameterivNV" glMapParameterivNV) :void GLenum GLenum :pointer)
  (("glMapParameterfvNV" glMapParameterfvNV) :void GLenum GLenum :pointer)
  (("glGetMapControlPointsNV" glGetMapControlPointsNV) :void GLenum GLuint
   GLenum GLsizei GLsizei GLboolean :pointer)
  (("glGetMapParameterivNV" glGetMapParameterivNV) :void GLenum GLenum
   :pointer)
  (("glGetMapParameterfvNV" glGetMapParameterfvNV) :void GLenum GLenum
   :pointer)
  (("glGetMapAttribParameterivNV" glGetMapAttribParameterivNV) :void GLenum
   GLuint GLenum :pointer)
  (("glGetMapAttribParameterfvNV" glGetMapAttribParameterfvNV) :void GLenum
   GLuint GLenum :pointer)
  (("glEvalMapsNV" glEvalMapsNV) :void GLenum GLenum))
(define-extension :gl_nv_packed_depth_stencil)
(define-extension :gl_nv_register_combiners2
  (("glCombinerStageParameterfvNV" glCombinerStageParameterfvNV) :void GLenum
   GLenum :pointer)
  (("glGetCombinerStageParameterfvNV" glGetCombinerStageParameterfvNV) :void
   GLenum GLenum :pointer))
(define-extension :gl_nv_texture_compression_vtc)
(define-extension :gl_nv_texture_rectangle)
(define-extension :gl_nv_texture_shader)
(define-extension :gl_nv_texture_shader2)
(define-extension :gl_nv_vertex_array_range2)
(define-extension :gl_nv_vertex_program
  (("glAreProgramsResidentNV" glAreProgramsResidentNV) GLboolean GLsizei
   :pointer :pointer)
  (("glBindProgramNV" glBindProgramNV) :void GLenum GLuint)
  (("glDeleteProgramsNV" glDeleteProgramsNV) :void GLsizei :pointer)
  (("glExecuteProgramNV" glExecuteProgramNV) :void GLenum GLuint :pointer)
  (("glGenProgramsNV" glGenProgramsNV) :void GLsizei :pointer)
  (("glGetProgramParameterdvNV" glGetProgramParameterdvNV) :void GLenum GLuint
   GLenum :pointer)
  (("glGetProgramParameterfvNV" glGetProgramParameterfvNV) :void GLenum GLuint
   GLenum :pointer)
  (("glGetProgramivNV" glGetProgramivNV) :void GLuint GLenum :pointer)
  (("glGetProgramStringNV" glGetProgramStringNV) :void GLuint GLenum :pointer)
  (("glGetTrackMatrixivNV" glGetTrackMatrixivNV) :void GLenum GLuint GLenum
   :pointer)
  (("glGetVertexAttribdvNV" glGetVertexAttribdvNV) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribfvNV" glGetVertexAttribfvNV) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribivNV" glGetVertexAttribivNV) :void GLuint GLenum
   :pointer)
  (("glGetVertexAttribPointervNV" glGetVertexAttribPointervNV) :void GLuint
   GLenum :pointer)
  (("glIsProgramNV" glIsProgramNV) GLboolean GLuint)
  (("glLoadProgramNV" glLoadProgramNV) :void GLenum GLuint GLsizei :pointer)
  (("glProgramParameter4dNV" glProgramParameter4dNV) :void GLenum GLuint
   GLdouble GLdouble GLdouble GLdouble)
  (("glProgramParameter4dvNV" glProgramParameter4dvNV) :void GLenum GLuint
   :pointer)
  (("glProgramParameter4fNV" glProgramParameter4fNV) :void GLenum GLuint
   GLfloat GLfloat GLfloat GLfloat)
  (("glProgramParameter4fvNV" glProgramParameter4fvNV) :void GLenum GLuint
   :pointer)
  (("glProgramParameters4dvNV" glProgramParameters4dvNV) :void GLenum GLuint
   GLuint :pointer)
  (("glProgramParameters4fvNV" glProgramParameters4fvNV) :void GLenum GLuint
   GLuint :pointer)
  (("glRequestResidentProgramsNV" glRequestResidentProgramsNV) :void GLsizei
   :pointer)
  (("glTrackMatrixNV" glTrackMatrixNV) :void GLenum GLuint GLenum GLenum)
  (("glVertexAttribPointerNV" glVertexAttribPointerNV) :void GLuint GLint
   GLenum GLsizei :pointer)
  (("glVertexAttrib1dNV" glVertexAttrib1dNV) :void GLuint GLdouble)
  (("glVertexAttrib1dvNV" glVertexAttrib1dvNV) :void GLuint :pointer)
  (("glVertexAttrib1fNV" glVertexAttrib1fNV) :void GLuint GLfloat)
  (("glVertexAttrib1fvNV" glVertexAttrib1fvNV) :void GLuint :pointer)
  (("glVertexAttrib1sNV" glVertexAttrib1sNV) :void GLuint GLshort)
  (("glVertexAttrib1svNV" glVertexAttrib1svNV) :void GLuint :pointer)
  (("glVertexAttrib2dNV" glVertexAttrib2dNV) :void GLuint GLdouble GLdouble)
  (("glVertexAttrib2dvNV" glVertexAttrib2dvNV) :void GLuint :pointer)
  (("glVertexAttrib2fNV" glVertexAttrib2fNV) :void GLuint GLfloat GLfloat)
  (("glVertexAttrib2fvNV" glVertexAttrib2fvNV) :void GLuint :pointer)
  (("glVertexAttrib2sNV" glVertexAttrib2sNV) :void GLuint GLshort GLshort)
  (("glVertexAttrib2svNV" glVertexAttrib2svNV) :void GLuint :pointer)
  (("glVertexAttrib3dNV" glVertexAttrib3dNV) :void GLuint GLdouble GLdouble
   GLdouble)
  (("glVertexAttrib3dvNV" glVertexAttrib3dvNV) :void GLuint :pointer)
  (("glVertexAttrib3fNV" glVertexAttrib3fNV) :void GLuint GLfloat GLfloat
   GLfloat)
  (("glVertexAttrib3fvNV" glVertexAttrib3fvNV) :void GLuint :pointer)
  (("glVertexAttrib3sNV" glVertexAttrib3sNV) :void GLuint GLshort GLshort
   GLshort)
  (("glVertexAttrib3svNV" glVertexAttrib3svNV) :void GLuint :pointer)
  (("glVertexAttrib4dNV" glVertexAttrib4dNV) :void GLuint GLdouble GLdouble
   GLdouble GLdouble)
  (("glVertexAttrib4dvNV" glVertexAttrib4dvNV) :void GLuint :pointer)
  (("glVertexAttrib4fNV" glVertexAttrib4fNV) :void GLuint GLfloat GLfloat
   GLfloat GLfloat)
  (("glVertexAttrib4fvNV" glVertexAttrib4fvNV) :void GLuint :pointer)
  (("glVertexAttrib4sNV" glVertexAttrib4sNV) :void GLuint GLshort GLshort
   GLshort GLshort)
  (("glVertexAttrib4svNV" glVertexAttrib4svNV) :void GLuint :pointer)
  (("glVertexAttrib4ubNV" glVertexAttrib4ubNV) :void GLuint GLubyte GLubyte
   GLubyte GLubyte)
  (("glVertexAttrib4ubvNV" glVertexAttrib4ubvNV) :void GLuint :pointer)
  (("glVertexAttribs1dvNV" glVertexAttribs1dvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs1fvNV" glVertexAttribs1fvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs1svNV" glVertexAttribs1svNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs2dvNV" glVertexAttribs2dvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs2fvNV" glVertexAttribs2fvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs2svNV" glVertexAttribs2svNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs3dvNV" glVertexAttribs3dvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs3fvNV" glVertexAttribs3fvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs3svNV" glVertexAttribs3svNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs4dvNV" glVertexAttribs4dvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs4fvNV" glVertexAttribs4fvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs4svNV" glVertexAttribs4svNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs4ubvNV" glVertexAttribs4ubvNV) :void GLuint GLsizei
   :pointer))
(define-extension :gl_sgix_texture_coordinate_clamp)
(define-extension :gl_sgix_scalebias_hint)
(define-extension :gl_oml_interlace)
(define-extension :gl_oml_subsample)
(define-extension :gl_oml_resample)
(define-extension :gl_nv_copy_depth_to_color)
(define-extension :gl_ati_envmap_bumpmap
  (("glTexBumpParameterivATI" glTexBumpParameterivATI) :void GLenum :pointer)
  (("glTexBumpParameterfvATI" glTexBumpParameterfvATI) :void GLenum :pointer)
  (("glGetTexBumpParameterivATI" glGetTexBumpParameterivATI) :void GLenum
   :pointer)
  (("glGetTexBumpParameterfvATI" glGetTexBumpParameterfvATI) :void GLenum
   :pointer))
(define-extension :gl_ati_fragment_shader
  (("glGenFragmentShadersATI" glGenFragmentShadersATI) GLuint GLuint)
  (("glBindFragmentShaderATI" glBindFragmentShaderATI) :void GLuint)
  (("glDeleteFragmentShaderATI" glDeleteFragmentShaderATI) :void GLuint)
  (("glBeginFragmentShaderATI" glBeginFragmentShaderATI) :void)
  (("glEndFragmentShaderATI" glEndFragmentShaderATI) :void)
  (("glPassTexCoordATI" glPassTexCoordATI) :void GLuint GLuint GLenum)
  (("glSampleMapATI" glSampleMapATI) :void GLuint GLuint GLenum)
  (("glColorFragmentOp1ATI" glColorFragmentOp1ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint GLuint)
  (("glColorFragmentOp2ATI" glColorFragmentOp2ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint GLuint GLuint GLuint GLuint)
  (("glColorFragmentOp3ATI" glColorFragmentOp3ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint GLuint GLuint GLuint GLuint GLuint GLuint GLuint)
  (("glAlphaFragmentOp1ATI" glAlphaFragmentOp1ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint)
  (("glAlphaFragmentOp2ATI" glAlphaFragmentOp2ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint GLuint GLuint GLuint)
  (("glAlphaFragmentOp3ATI" glAlphaFragmentOp3ATI) :void GLenum GLuint GLuint
   GLuint GLuint GLuint GLuint GLuint GLuint GLuint GLuint GLuint)
  (("glSetFragmentShaderConstantATI" glSetFragmentShaderConstantATI) :void
   GLuint :pointer))
(define-extension :gl_ati_pn_triangles
  (("glPNTrianglesiATI" glPNTrianglesiATI) :void GLenum GLint)
  (("glPNTrianglesfATI" glPNTrianglesfATI) :void GLenum GLfloat))
(define-extension :gl_ati_vertex_array_object
  (("glNewObjectBufferATI" glNewObjectBufferATI) GLuint GLsizei :pointer
   GLenum)
  (("glIsObjectBufferATI" glIsObjectBufferATI) GLboolean GLuint)
  (("glUpdateObjectBufferATI" glUpdateObjectBufferATI) :void GLuint GLuint
   GLsizei :pointer GLenum)
  (("glGetObjectBufferfvATI" glGetObjectBufferfvATI) :void GLuint GLenum
   :pointer)
  (("glGetObjectBufferivATI" glGetObjectBufferivATI) :void GLuint GLenum
   :pointer)
  (("glFreeObjectBufferATI" glFreeObjectBufferATI) :void GLuint)
  (("glArrayObjectATI" glArrayObjectATI) :void GLenum GLint GLenum GLsizei
   GLuint GLuint)
  (("glGetArrayObjectfvATI" glGetArrayObjectfvATI) :void GLenum GLenum
   :pointer)
  (("glGetArrayObjectivATI" glGetArrayObjectivATI) :void GLenum GLenum
   :pointer)
  (("glVariantArrayObjectATI" glVariantArrayObjectATI) :void GLuint GLenum
   GLsizei GLuint GLuint)
  (("glGetVariantArrayObjectfvATI" glGetVariantArrayObjectfvATI) :void GLuint
   GLenum :pointer)
  (("glGetVariantArrayObjectivATI" glGetVariantArrayObjectivATI) :void GLuint
   GLenum :pointer))
(define-extension :gl_ext_vertex_shader
  (("glBeginVertexShaderEXT" glBeginVertexShaderEXT) :void)
  (("glEndVertexShaderEXT" glEndVertexShaderEXT) :void)
  (("glBindVertexShaderEXT" glBindVertexShaderEXT) :void GLuint)
  (("glGenVertexShadersEXT" glGenVertexShadersEXT) GLuint GLuint)
  (("glDeleteVertexShaderEXT" glDeleteVertexShaderEXT) :void GLuint)
  (("glShaderOp1EXT" glShaderOp1EXT) :void GLenum GLuint GLuint)
  (("glShaderOp2EXT" glShaderOp2EXT) :void GLenum GLuint GLuint GLuint)
  (("glShaderOp3EXT" glShaderOp3EXT) :void GLenum GLuint GLuint GLuint GLuint)
  (("glSwizzleEXT" glSwizzleEXT) :void GLuint GLuint GLenum GLenum GLenum
   GLenum)
  (("glWriteMaskEXT" glWriteMaskEXT) :void GLuint GLuint GLenum GLenum GLenum
   GLenum)
  (("glInsertComponentEXT" glInsertComponentEXT) :void GLuint GLuint GLuint)
  (("glExtractComponentEXT" glExtractComponentEXT) :void GLuint GLuint GLuint)
  (("glGenSymbolsEXT" glGenSymbolsEXT) GLuint GLenum GLenum GLenum GLuint)
  (("glSetInvariantEXT" glSetInvariantEXT) :void GLuint GLenum :pointer)
  (("glSetLocalConstantEXT" glSetLocalConstantEXT) :void GLuint GLenum
   :pointer)
  (("glVariantbvEXT" glVariantbvEXT) :void GLuint :pointer)
  (("glVariantsvEXT" glVariantsvEXT) :void GLuint :pointer)
  (("glVariantivEXT" glVariantivEXT) :void GLuint :pointer)
  (("glVariantfvEXT" glVariantfvEXT) :void GLuint :pointer)
  (("glVariantdvEXT" glVariantdvEXT) :void GLuint :pointer)
  (("glVariantubvEXT" glVariantubvEXT) :void GLuint :pointer)
  (("glVariantusvEXT" glVariantusvEXT) :void GLuint :pointer)
  (("glVariantuivEXT" glVariantuivEXT) :void GLuint :pointer)
  (("glVariantPointerEXT" glVariantPointerEXT) :void GLuint GLenum GLuint
   :pointer)
  (("glEnableVariantClientStateEXT" glEnableVariantClientStateEXT) :void
   GLuint)
  (("glDisableVariantClientStateEXT" glDisableVariantClientStateEXT) :void
   GLuint)
  (("glBindLightParameterEXT" glBindLightParameterEXT) GLuint GLenum GLenum)
  (("glBindMaterialParameterEXT" glBindMaterialParameterEXT) GLuint GLenum
   GLenum)
  (("glBindTexGenParameterEXT" glBindTexGenParameterEXT) GLuint GLenum GLenum
   GLenum)
  (("glBindTextureUnitParameterEXT" glBindTextureUnitParameterEXT) GLuint
   GLenum GLenum)
  (("glBindParameterEXT" glBindParameterEXT) GLuint GLenum)
  (("glIsVariantEnabledEXT" glIsVariantEnabledEXT) GLboolean GLuint GLenum)
  (("glGetVariantBooleanvEXT" glGetVariantBooleanvEXT) :void GLuint GLenum
   :pointer)
  (("glGetVariantIntegervEXT" glGetVariantIntegervEXT) :void GLuint GLenum
   :pointer)
  (("glGetVariantFloatvEXT" glGetVariantFloatvEXT) :void GLuint GLenum
   :pointer)
  (("glGetVariantPointervEXT" glGetVariantPointervEXT) :void GLuint GLenum
   :pointer)
  (("glGetInvariantBooleanvEXT" glGetInvariantBooleanvEXT) :void GLuint GLenum
   :pointer)
  (("glGetInvariantIntegervEXT" glGetInvariantIntegervEXT) :void GLuint GLenum
   :pointer)
  (("glGetInvariantFloatvEXT" glGetInvariantFloatvEXT) :void GLuint GLenum
   :pointer)
  (("glGetLocalConstantBooleanvEXT" glGetLocalConstantBooleanvEXT) :void GLuint
   GLenum :pointer)
  (("glGetLocalConstantIntegervEXT" glGetLocalConstantIntegervEXT) :void GLuint
   GLenum :pointer)
  (("glGetLocalConstantFloatvEXT" glGetLocalConstantFloatvEXT) :void GLuint
   GLenum :pointer))
(define-extension :gl_ati_vertex_streams
  (("glVertexStream1sATI" glVertexStream1sATI) :void GLenum GLshort)
  (("glVertexStream1svATI" glVertexStream1svATI) :void GLenum :pointer)
  (("glVertexStream1iATI" glVertexStream1iATI) :void GLenum GLint)
  (("glVertexStream1ivATI" glVertexStream1ivATI) :void GLenum :pointer)
  (("glVertexStream1fATI" glVertexStream1fATI) :void GLenum GLfloat)
  (("glVertexStream1fvATI" glVertexStream1fvATI) :void GLenum :pointer)
  (("glVertexStream1dATI" glVertexStream1dATI) :void GLenum GLdouble)
  (("glVertexStream1dvATI" glVertexStream1dvATI) :void GLenum :pointer)
  (("glVertexStream2sATI" glVertexStream2sATI) :void GLenum GLshort GLshort)
  (("glVertexStream2svATI" glVertexStream2svATI) :void GLenum :pointer)
  (("glVertexStream2iATI" glVertexStream2iATI) :void GLenum GLint GLint)
  (("glVertexStream2ivATI" glVertexStream2ivATI) :void GLenum :pointer)
  (("glVertexStream2fATI" glVertexStream2fATI) :void GLenum GLfloat GLfloat)
  (("glVertexStream2fvATI" glVertexStream2fvATI) :void GLenum :pointer)
  (("glVertexStream2dATI" glVertexStream2dATI) :void GLenum GLdouble GLdouble)
  (("glVertexStream2dvATI" glVertexStream2dvATI) :void GLenum :pointer)
  (("glVertexStream3sATI" glVertexStream3sATI) :void GLenum GLshort GLshort
   GLshort)
  (("glVertexStream3svATI" glVertexStream3svATI) :void GLenum :pointer)
  (("glVertexStream3iATI" glVertexStream3iATI) :void GLenum GLint GLint GLint)
  (("glVertexStream3ivATI" glVertexStream3ivATI) :void GLenum :pointer)
  (("glVertexStream3fATI" glVertexStream3fATI) :void GLenum GLfloat GLfloat
   GLfloat)
  (("glVertexStream3fvATI" glVertexStream3fvATI) :void GLenum :pointer)
  (("glVertexStream3dATI" glVertexStream3dATI) :void GLenum GLdouble GLdouble
   GLdouble)
  (("glVertexStream3dvATI" glVertexStream3dvATI) :void GLenum :pointer)
  (("glVertexStream4sATI" glVertexStream4sATI) :void GLenum GLshort GLshort
   GLshort GLshort)
  (("glVertexStream4svATI" glVertexStream4svATI) :void GLenum :pointer)
  (("glVertexStream4iATI" glVertexStream4iATI) :void GLenum GLint GLint GLint
   GLint)
  (("glVertexStream4ivATI" glVertexStream4ivATI) :void GLenum :pointer)
  (("glVertexStream4fATI" glVertexStream4fATI) :void GLenum GLfloat GLfloat
   GLfloat GLfloat)
  (("glVertexStream4fvATI" glVertexStream4fvATI) :void GLenum :pointer)
  (("glVertexStream4dATI" glVertexStream4dATI) :void GLenum GLdouble GLdouble
   GLdouble GLdouble)
  (("glVertexStream4dvATI" glVertexStream4dvATI) :void GLenum :pointer)
  (("glNormalStream3bATI" glNormalStream3bATI) :void GLenum GLbyte GLbyte
   GLbyte)
  (("glNormalStream3bvATI" glNormalStream3bvATI) :void GLenum :pointer)
  (("glNormalStream3sATI" glNormalStream3sATI) :void GLenum GLshort GLshort
   GLshort)
  (("glNormalStream3svATI" glNormalStream3svATI) :void GLenum :pointer)
  (("glNormalStream3iATI" glNormalStream3iATI) :void GLenum GLint GLint GLint)
  (("glNormalStream3ivATI" glNormalStream3ivATI) :void GLenum :pointer)
  (("glNormalStream3fATI" glNormalStream3fATI) :void GLenum GLfloat GLfloat
   GLfloat)
  (("glNormalStream3fvATI" glNormalStream3fvATI) :void GLenum :pointer)
  (("glNormalStream3dATI" glNormalStream3dATI) :void GLenum GLdouble GLdouble
   GLdouble)
  (("glNormalStream3dvATI" glNormalStream3dvATI) :void GLenum :pointer)
  (("glClientActiveVertexStreamATI" glClientActiveVertexStreamATI) :void
   GLenum)
  (("glVertexBlendEnviATI" glVertexBlendEnviATI) :void GLenum GLint)
  (("glVertexBlendEnvfATI" glVertexBlendEnvfATI) :void GLenum GLfloat))
(define-extension :gl_ati_element_array
  (("glElementPointerATI" glElementPointerATI) :void GLenum :pointer)
  (("glDrawElementArrayATI" glDrawElementArrayATI) :void GLenum GLsizei)
  (("glDrawRangeElementArrayATI" glDrawRangeElementArrayATI) :void GLenum
   GLuint GLuint GLsizei))
(define-extension :gl_sun_mesh_array
  (("glDrawMeshArraysSUN" glDrawMeshArraysSUN) :void GLenum GLint GLsizei
   GLsizei))
(define-extension :gl_sun_slice_accum)
(define-extension :gl_nv_multisample_filter_hint)
(define-extension :gl_nv_depth_clamp)
(define-extension :gl_nv_occlusion_query
  (("glGenOcclusionQueriesNV" glGenOcclusionQueriesNV) :void GLsizei :pointer)
  (("glDeleteOcclusionQueriesNV" glDeleteOcclusionQueriesNV) :void GLsizei
   :pointer)
  (("glIsOcclusionQueryNV" glIsOcclusionQueryNV) GLboolean GLuint)
  (("glBeginOcclusionQueryNV" glBeginOcclusionQueryNV) :void GLuint)
  (("glEndOcclusionQueryNV" glEndOcclusionQueryNV) :void)
  (("glGetOcclusionQueryivNV" glGetOcclusionQueryivNV) :void GLuint GLenum
   :pointer)
  (("glGetOcclusionQueryuivNV" glGetOcclusionQueryuivNV) :void GLuint GLenum
   :pointer))
(define-extension :gl_nv_point_sprite
  (("glPointParameteriNV" glPointParameteriNV) :void GLenum GLint)
  (("glPointParameterivNV" glPointParameterivNV) :void GLenum :pointer))
(define-extension :gl_nv_texture_shader3)
(define-extension :gl_nv_vertex_program1_1)
(define-extension :gl_ext_shadow_funcs)
(define-extension :gl_ext_stencil_two_side
  (("glActiveStencilFaceEXT" glActiveStencilFaceEXT) :void GLenum))
(define-extension :gl_ati_text_fragment_shader)
(define-extension :gl_apple_client_storage)
(define-extension :gl_apple_element_array
  (("glElementPointerAPPLE" glElementPointerAPPLE) :void GLenum :pointer)
  (("glDrawElementArrayAPPLE" glDrawElementArrayAPPLE) :void GLenum GLint
   GLsizei)
  (("glDrawRangeElementArrayAPPLE" glDrawRangeElementArrayAPPLE) :void GLenum
   GLuint GLuint GLint GLsizei)
  (("glMultiDrawElementArrayAPPLE" glMultiDrawElementArrayAPPLE) :void GLenum
   :pointer :pointer GLsizei)
  (("glMultiDrawRangeElementArrayAPPLE" glMultiDrawRangeElementArrayAPPLE)
   :void GLenum GLuint GLuint :pointer :pointer GLsizei))
(define-extension :gl_apple_fence
  (("glGenFencesAPPLE" glGenFencesAPPLE) :void GLsizei :pointer)
  (("glDeleteFencesAPPLE" glDeleteFencesAPPLE) :void GLsizei :pointer)
  (("glSetFenceAPPLE" glSetFenceAPPLE) :void GLuint)
  (("glIsFenceAPPLE" glIsFenceAPPLE) GLboolean GLuint)
  (("glTestFenceAPPLE" glTestFenceAPPLE) GLboolean GLuint)
  (("glFinishFenceAPPLE" glFinishFenceAPPLE) :void GLuint)
  (("glTestObjectAPPLE" glTestObjectAPPLE) GLboolean GLenum GLuint)
  (("glFinishObjectAPPLE" glFinishObjectAPPLE) :void GLenum GLint))
(define-extension :gl_apple_vertex_array_object
  (("glBindVertexArrayAPPLE" glBindVertexArrayAPPLE) :void GLuint)
  (("glDeleteVertexArraysAPPLE" glDeleteVertexArraysAPPLE) :void GLsizei
   :pointer)
  (("glGenVertexArraysAPPLE" glGenVertexArraysAPPLE) :void GLsizei :pointer)
  (("glIsVertexArrayAPPLE" glIsVertexArrayAPPLE) GLboolean GLuint))
(define-extension :gl_apple_vertex_array_range
  (("glVertexArrayRangeAPPLE" glVertexArrayRangeAPPLE) :void GLsizei :pointer)
  (("glFlushVertexArrayRangeAPPLE" glFlushVertexArrayRangeAPPLE) :void GLsizei
   :pointer)
  (("glVertexArrayParameteriAPPLE" glVertexArrayParameteriAPPLE) :void GLenum
   GLint))
(define-extension :gl_apple_ycbcr_422)
(define-extension :gl_s3_s3tc)
(define-extension :gl_ati_draw_buffers
  (("glDrawBuffersATI" glDrawBuffersATI) :void GLsizei :pointer))
(define-extension :gl_ati_pixel_format_float)
(define-extension :gl_ati_texture_env_combine3)
(define-extension :gl_ati_texture_float)
(define-extension :gl_nv_float_buffer)
(define-extension :gl_nv_fragment_program
  (("glProgramNamedParameter4fNV" glProgramNamedParameter4fNV) :void GLuint
   GLsizei :pointer GLfloat GLfloat GLfloat GLfloat)
  (("glProgramNamedParameter4dNV" glProgramNamedParameter4dNV) :void GLuint
   GLsizei :pointer GLdouble GLdouble GLdouble GLdouble)
  (("glProgramNamedParameter4fvNV" glProgramNamedParameter4fvNV) :void GLuint
   GLsizei :pointer :pointer)
  (("glProgramNamedParameter4dvNV" glProgramNamedParameter4dvNV) :void GLuint
   GLsizei :pointer :pointer)
  (("glGetProgramNamedParameterfvNV" glGetProgramNamedParameterfvNV) :void
   GLuint GLsizei :pointer :pointer)
  (("glGetProgramNamedParameterdvNV" glGetProgramNamedParameterdvNV) :void
   GLuint GLsizei :pointer :pointer))
(define-extension :gl_nv_half_float
  (("glVertex2hNV" glVertex2hNV) :void GLhalfnv GLhalfnv)
  (("glVertex2hvNV" glVertex2hvNV) :void :pointer)
  (("glVertex3hNV" glVertex3hNV) :void GLhalfnv GLhalfnv GLhalfnv)
  (("glVertex3hvNV" glVertex3hvNV) :void :pointer)
  (("glVertex4hNV" glVertex4hNV) :void GLhalfnv GLhalfnv GLhalfnv GLhalfnv)
  (("glVertex4hvNV" glVertex4hvNV) :void :pointer)
  (("glNormal3hNV" glNormal3hNV) :void GLhalfnv GLhalfnv GLhalfnv)
  (("glNormal3hvNV" glNormal3hvNV) :void :pointer)
  (("glColor3hNV" glColor3hNV) :void GLhalfnv GLhalfnv GLhalfnv)
  (("glColor3hvNV" glColor3hvNV) :void :pointer)
  (("glColor4hNV" glColor4hNV) :void GLhalfnv GLhalfnv GLhalfnv GLhalfnv)
  (("glColor4hvNV" glColor4hvNV) :void :pointer)
  (("glTexCoord1hNV" glTexCoord1hNV) :void GLhalfnv)
  (("glTexCoord1hvNV" glTexCoord1hvNV) :void :pointer)
  (("glTexCoord2hNV" glTexCoord2hNV) :void GLhalfnv GLhalfnv)
  (("glTexCoord2hvNV" glTexCoord2hvNV) :void :pointer)
  (("glTexCoord3hNV" glTexCoord3hNV) :void GLhalfnv GLhalfnv GLhalfnv)
  (("glTexCoord3hvNV" glTexCoord3hvNV) :void :pointer)
  (("glTexCoord4hNV" glTexCoord4hNV) :void GLhalfnv GLhalfnv GLhalfnv GLhalfnv)
  (("glTexCoord4hvNV" glTexCoord4hvNV) :void :pointer)
  (("glMultiTexCoord1hNV" glMultiTexCoord1hNV) :void GLenum GLhalfnv)
  (("glMultiTexCoord1hvNV" glMultiTexCoord1hvNV) :void GLenum :pointer)
  (("glMultiTexCoord2hNV" glMultiTexCoord2hNV) :void GLenum GLhalfnv GLhalfnv)
  (("glMultiTexCoord2hvNV" glMultiTexCoord2hvNV) :void GLenum :pointer)
  (("glMultiTexCoord3hNV" glMultiTexCoord3hNV) :void GLenum GLhalfnv GLhalfnv
   GLhalfnv)
  (("glMultiTexCoord3hvNV" glMultiTexCoord3hvNV) :void GLenum :pointer)
  (("glMultiTexCoord4hNV" glMultiTexCoord4hNV) :void GLenum GLhalfnv GLhalfnv
   GLhalfnv GLhalfnv)
  (("glMultiTexCoord4hvNV" glMultiTexCoord4hvNV) :void GLenum :pointer)
  (("glFogCoordhNV" glFogCoordhNV) :void GLhalfnv)
  (("glFogCoordhvNV" glFogCoordhvNV) :void :pointer)
  (("glSecondaryColor3hNV" glSecondaryColor3hNV) :void GLhalfnv GLhalfnv
   GLhalfnv)
  (("glSecondaryColor3hvNV" glSecondaryColor3hvNV) :void :pointer)
  (("glVertexWeighthNV" glVertexWeighthNV) :void GLhalfnv)
  (("glVertexWeighthvNV" glVertexWeighthvNV) :void :pointer)
  (("glVertexAttrib1hNV" glVertexAttrib1hNV) :void GLuint GLhalfnv)
  (("glVertexAttrib1hvNV" glVertexAttrib1hvNV) :void GLuint :pointer)
  (("glVertexAttrib2hNV" glVertexAttrib2hNV) :void GLuint GLhalfnv GLhalfnv)
  (("glVertexAttrib2hvNV" glVertexAttrib2hvNV) :void GLuint :pointer)
  (("glVertexAttrib3hNV" glVertexAttrib3hNV) :void GLuint GLhalfnv GLhalfnv
   GLhalfnv)
  (("glVertexAttrib3hvNV" glVertexAttrib3hvNV) :void GLuint :pointer)
  (("glVertexAttrib4hNV" glVertexAttrib4hNV) :void GLuint GLhalfnv GLhalfnv
   GLhalfnv GLhalfnv)
  (("glVertexAttrib4hvNV" glVertexAttrib4hvNV) :void GLuint :pointer)
  (("glVertexAttribs1hvNV" glVertexAttribs1hvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs2hvNV" glVertexAttribs2hvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs3hvNV" glVertexAttribs3hvNV) :void GLuint GLsizei :pointer)
  (("glVertexAttribs4hvNV" glVertexAttribs4hvNV) :void GLuint GLsizei :pointer))
(define-extension :gl_nv_pixel_data_range
  (("glPixelDataRangeNV" glPixelDataRangeNV) :void GLenum GLsizei :pointer)
  (("glFlushPixelDataRangeNV" glFlushPixelDataRangeNV) :void GLenum))
(define-extension :gl_nv_primitive_restart
  (("glPrimitiveRestartNV" glPrimitiveRestartNV) :void)
  (("glPrimitiveRestartIndexNV" glPrimitiveRestartIndexNV) :void GLuint))
(define-extension :gl_nv_texture_expand_normal)
(define-extension :gl_nv_vertex_program2)
(define-extension :gl_ati_map_object_buffer
  (("glMapObjectBufferATI" glMapObjectBufferATI) :pointer GLuint)
  (("glUnmapObjectBufferATI" glUnmapObjectBufferATI) :void GLuint))
(define-extension :gl_ati_separate_stencil
  (("glStencilOpSeparateATI" glStencilOpSeparateATI) :void GLenum GLenum GLenum
   GLenum)
  (("glStencilFuncSeparateATI" glStencilFuncSeparateATI) :void GLenum GLenum
   GLint GLuint))
(define-extension :gl_ati_vertex_attrib_array_object
  (("glVertexAttribArrayObjectATI" glVertexAttribArrayObjectATI) :void GLuint
   GLint GLenum GLboolean GLsizei GLuint GLuint)
  (("glGetVertexAttribArrayObjectfvATI" glGetVertexAttribArrayObjectfvATI)
   :void GLuint GLenum :pointer)
  (("glGetVertexAttribArrayObjectivATI" glGetVertexAttribArrayObjectivATI)
   :void GLuint GLenum :pointer))
(define-extension :gl_ext_depth_bounds_test
  (("glDepthBoundsEXT" glDepthBoundsEXT) :void GLclampd GLclampd))
(define-extension :gl_ext_texture_mirror_clamp)
(define-extension :gl_ext_blend_equation_separate
  (("glBlendEquationSeparateEXT" glBlendEquationSeparateEXT) :void GLenum
   GLenum))
(define-extension :gl_mesa_pack_invert)
(define-extension :gl_mesa_ycbcr_texture)
(define-extension :gl_ext_pixel_buffer_object)
(define-extension :gl_nv_fragment_program_option)
(define-extension :gl_nv_fragment_program2)
(define-extension :gl_nv_vertex_program2_option)
(define-extension :gl_nv_vertex_program3)
