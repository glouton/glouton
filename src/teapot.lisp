(in-package :glouton)

;;; Copyright (c) 2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
       
;;; This data comes from Steve Baker's website, http://www.sjbaker.org/teapot/.
;;; The teapot is defined in a Z up coordinate system. All the patches are
;;; defined in the (+x, -y) quadrant and then reflected about the other planes
;;; as necessary.
;;;
;;; The GLUT teapot code produces quads whose front faces are defined in the
;;; clockwise direction. This code produces quads in the usual
;;; counter-clockwise direction. If you are following example C code that uses
;;; GLUT, you will want to eliminate calls to glFrontFace(GL_CW) or other
;;; lossage like turning off back-face culling.

;;; First element indicates if object needs to be reflected 4 times or twice.
(defparameter *teapot-indexes*
  '(
    ;; Rim:
    (t 102 103 104 105 4 5 6 7 8 9 10 11 12 13 14 15)
    ;; Body:
    (t 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27)
    (t 24 25 26 27 29 30 31 32 33 34 35 36 37 38 39 40)
    ;; Lid:
    (t 96 96 96 96 97 98 99 100 101 101 101 101 0 1 2 3)
    (t 0 1 2 3 106 107 108 109 110 111 112 113 114 115 116 117)
    ;; Handle:
    (nil 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56)
    (nil 53 54 55 56 57 58 59 60 61 62 63 64 28 65 66 67)
    ;; Spout:
    (nil 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83)
    (nil 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95)
    ;; Bottom:
    (t 118 118 118 118 124 122 119 121 123 126 125 120 40 39 38 37)))

(defparameter *teapot-points*
  (make-array
   '(127 3) :element-type 'single-float
   :initial-contents
   '(
     (0.2000 0.0000 2.70000) (0.2000 -0.1120 2.70000) (0.1120 -0.2000 2.70000) 
     (0.0000 -0.2000 2.70000) (1.3375 0.0000 2.53125) (1.3375 -0.7490 2.53125) 
     (0.7490 -1.3375 2.53125) (0.0000 -1.3375 2.53125) (1.4375 0.0000 2.53125) 
     (1.4375 -0.8050 2.53125) (0.8050 -1.4375 2.53125) (0.0000 -1.4375 2.53125) 
     (1.5000 0.0000 2.40000) (1.5000 -0.8400 2.40000) (0.8400 -1.5000 2.40000) 
     (0.0000 -1.5000 2.40000) (1.7500 0.0000 1.87500) (1.7500 -0.9800 1.87500) 
     (0.9800 -1.7500 1.87500) (0.0000 -1.7500 1.87500) (2.0000 0.0000 1.35000) 
     (2.0000 -1.1200 1.35000) (1.1200 -2.0000 1.35000) (0.0000 -2.0000 1.35000) 
     (2.0000 0.0000 0.90000) (2.0000 -1.1200 0.90000) (1.1200 -2.0000 0.90000) 
     (0.0000 -2.0000 0.90000) (-2.0000 0.0000 0.90000) (2.0000 0.0000 0.45000) 
     (2.0000 -1.1200 0.45000) (1.1200 -2.0000 0.45000) (0.0000 -2.0000 0.45000) 
     (1.5000 0.0000 0.22500) (1.5000 -0.8400 0.22500) (0.8400 -1.5000 0.22500) 
     (0.0000 -1.5000 0.22500) (1.5000 0.0000 0.15000) (1.5000 -0.8400 0.15000) 
     (0.8400 -1.5000 0.15000) (0.0000 -1.5000 0.15000) (-1.6000 0.0000 2.02500) 
     (-1.6000 -0.3000 2.02500) (-1.5000 -0.3000 2.25000) (-1.5000 0.0000 2.25000) 
     (-2.3000 0.0000 2.02500) (-2.3000 -0.3000 2.02500) (-2.5000 -0.3000 2.25000) 
     (-2.5000 0.0000 2.25000) (-2.7000 0.0000 2.02500) (-2.7000 -0.3000 2.02500) 
     (-3.0000 -0.3000 2.25000) (-3.0000 0.0000 2.25000) (-2.7000 0.0000 1.80000) 
     (-2.7000 -0.3000 1.80000) (-3.0000 -0.3000 1.80000) (-3.0000 0.0000 1.80000) 
     (-2.7000 0.0000 1.57500) (-2.7000 -0.3000 1.57500) (-3.0000 -0.3000 1.35000) 
     (-3.0000 0.0000 1.35000) (-2.5000 0.0000 1.12500) (-2.5000 -0.3000 1.12500) 
     (-2.6500 -0.3000 0.93750) (-2.6500 0.0000 0.93750) (-2.0000 -0.3000 0.90000) 
     (-1.9000 -0.3000 0.60000) (-1.9000 0.0000 0.60000) (1.7000 0.0000 1.42500) 
     (1.7000 -0.6600 1.42500) (1.7000 -0.6600 0.60000) (1.7000 0.0000 0.60000) 
     (2.6000 0.0000 1.42500) (2.6000 -0.6600 1.42500) (3.1000 -0.6600 0.82500) 
     (3.1000 0.0000 0.82500) (2.3000 0.0000 2.10000) (2.3000 -0.2500 2.10000) 
     (2.4000 -0.2500 2.02500) (2.4000 0.0000 2.02500) (2.7000 0.0000 2.40000) 
     (2.7000 -0.2500 2.40000) (3.3000 -0.2500 2.40000) (3.3000 0.0000 2.40000) 
     (2.8000 0.0000 2.47500) (2.8000 -0.2500 2.47500) (3.5250 -0.2500 2.49375) 
     (3.5250 0.0000 2.49375) (2.9000 0.0000 2.47500) (2.9000 -0.1500 2.47500) 
     (3.4500 -0.1500 2.51250) (3.4500 0.0000 2.51250) (2.8000 0.0000 2.40000) 
     (2.8000 -0.1500 2.40000) (3.2000 -0.1500 2.40000) (3.2000 0.0000 2.40000) 
     (0.0000 0.0000 3.15000) (0.8000 0.0000 3.15000) (0.8000 -0.4500 3.15000) 
     (0.4500 -0.8000 3.15000) (0.0000 -0.8000 3.15000) (0.0000 0.0000 2.85000) 
     (1.4000 0.0000 2.40000) (1.4000 -0.7840 2.40000) (0.7840 -1.4000 2.40000) 
     (0.0000 -1.4000 2.40000) (0.4000 0.0000 2.55000) (0.4000 -0.2240 2.55000) 
     (0.2240 -0.4000 2.55000) (0.0000 -0.4000 2.55000) (1.3000 0.0000 2.55000) 
     (1.3000 -0.7280 2.55000) (0.7280 -1.3000 2.55000) (0.0000 -1.3000 2.55000) 
     (1.3000 0.0000 2.40000) (1.3000 -0.7280 2.40000) (0.7280 -1.3000 2.40000) 
     (0.0000 -1.3000 2.40000) (0.0000 0.0000 0.00000) (1.4250 -0.7980 0.00000)
     (1.5000 0.0000 0.07500) (1.4250 0.0000 0.00000) (0.7980 -1.4250 0.00000)
     (0.0000 -1.5000 0.07500)(0.0000 -1.4250 0.00000) (1.5000 -0.8400 0.07500)
     (0.8400 -1.5000 0.07500))))

(defvar *teapot-patches* nil)

;;; utilities for copying flipped array to foreign array

(defun flip (foreign grid x-fac y-fac)
  (dotimes (i 4)
    (dotimes (j 4)
      (let ((base (+ (* i 12) (* j 3))))
	(setf (cffi:mem-aref foreign :float base)
	      (* (aref grid i (- 3 j) 0)  x-fac))
	(setf (cffi:mem-aref foreign :float (+ base 1))
	      (* (aref grid i (- 3 j) 1) y-fac))
	(setf (cffi:mem-aref foreign :float (+ base 2))
	      (aref grid i (- 3 j) 2))))))
(loop
   for (reflect . patch-indexes) in *teapot-indexes*
   for coords-list = (mapcan #'(lambda (idx)
				(loop
				   for i from 0 below 3
				   collect (aref *teapot-points* idx i)))
			    patch-indexes)
   for coords = (coerce coords-list 'vector)
   ;; The patches
   ;; Tricks for mapping the patches around
   for grid = (make-array '(4 4 3) :displaced-to coords)
   for p = (cffi:foreign-alloc :float :initial-contents coords)
   for q = (cffi:foreign-alloc :float :count (* 4 4 3))
   for r = (and reflect (cffi:foreign-alloc :float :count (* 4 4 3)))
   for s = (and reflect
		(cffi:foreign-alloc
		 :float
		 :initial-contents (loop
				      for (x y z) on coords-list by #'cdddr
				      append (list (* -1.0 x) (* -1.0 y) z))))
   do (progn
	(flip q grid 1.0 -1.0)
	(when reflect
	  (flip r grid -1.0 1.0)))
     
   collect (list* p q (and reflect (list r s))) into result
   finally (setf *teapot-patches* result))

(defvar *tex* (cffi:foreign-alloc :float
				  :initial-contents '(0.0 0.0
						      1.0 0.0
						      0.0 1.0
						      1.0 1.0)))

(defgeneric render (patch grid scale type))

(defclass patch-list ()
  ((u-order :accessor u-order :initarg :x-order)
   (v-order :accessor v-order :initarg :y-order)
   (patches :accessor patches :initarg :patches))
  (:default-initargs :x-order 4 :y-order 4 :patches nil))

(defmethod render ((patch patch-list) grid scale type)
  (declare (ignore scale))
  (glPushAttrib (logior GL_ENABLE_BIT GL_EVAL_BIT))
  (glEnable GL_AUTO_NORMAL)
  (glEnable GL_NORMALIZE)
  (glEnable GL_MAP2_VERTEX_3)
  (glEnable GL_MAP2_TEXTURE_COORD_2)
  (glMap2f GL_MAP2_TEXTURE_COORD_2 0.0 1.0 2 2 0.0 1.0 4 2 *tex*)
  (glMapGrid2f grid 0.0 1.0 grid 0.0 1.0)
  (loop
     with u-order = (u-order patch)
     and v-order = (v-order patch)
     for patch-list in (patches patch)
     do (loop
	   for patch in patch-list
	   do (glMap2f GL_MAP2_VERTEX_3 0.0 1.0 3 u-order 1.0 0.0 12 v-order
		       patch)
	      (glEvalMesh2 type 0 grid 0 grid)))
  (glPopAttrib))

(defclass teapot (patch-list)
  ()
  (:default-initargs :patches *teapot-patches*))

(defmethod render :around ((patch teapot) grid scale type)
  (glPushMatrix)
  (glRotatef 270.0 1.0 0.0 0.0)
  (let ((scale (* .5 scale)))
    (glScalef scale scale scale))
  (glTranslatef 0.0 0.0 -1.5)
  (call-next-method)
  (glPopMatrix))
