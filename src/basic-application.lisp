;;; -*- Mode: Lisp -*-

;;; Copyright (c) 2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :glouton)


(defgeneric handle-exposure
    (event-loop event-key &key &allow-other-keys)
  (:documentation "This will be passed :window :send-event-p :x :y :width :height :count
 among others"))

(defgeneric handle-configure-notify
    (event-loop event-key &key &allow-other-keys)
  (:documentation "This may be passed :window :send-event-p :above-sibling :x :y :width
:height :border-width :override-redirect-p among others"))

(defgeneric handle-button (event-loop event-key &key &allow-other-keys)
  (:documentation "This will be passed :send-event-p :code :time :child :root-x
:root-y :x :y :state :same-screen-p among others"))

(defgeneric handle-key (event-loop event-key &key &allow-other-keys)
  (:documentation "This will be passed :send-event-p :code :time :child :root-x
:root-y :x :y :state :same-screen-p among others"))

(defgeneric handle-destroy (event-loop display window))

(defclass event-loop ()
  ((display :accessor display :initarg :display)
   (event-handlers :accessor event-handlers))
  (:default-initargs :display nil))

(defmethod initialize-instance :after ((obj event-loop) &key)
  (declare (optimize (speed 3) (safety 1) (debug 1))) ; for the benefit of sbcl
  (flet ((default-handler (&rest args)
	   (declare (dynamic-extent args))
	   (format *error-output* "unhandled event ~S~%" args))
	 (%exposure (&rest args &key event-key &allow-other-keys)
	   (declare (dynamic-extent args))
	   (apply #'handle-exposure obj event-key args))
	 (%configure-notify (&rest args &key event-key &allow-other-keys)
	   (declare (dynamic-extent args))
	   (apply #'handle-configure-notify obj event-key args))
	 (%button (&rest args &key event-key &allow-other-keys)
	   (declare (dynamic-extent args))
	   (apply #'handle-button obj event-key args))
	 (%key (&rest args &key event-key &allow-other-keys)
	   (apply #'handle-key obj event-key args)))
    (let ((handlers (xlib:make-event-handlers :type 'vector
					      :default #'default-handler)))
      (setf (xlib:event-handler handlers :exposure) #'%exposure)
      (setf (xlib:event-handler handlers :configure-notify)
	    #'%configure-notify)
      (setf (xlib:event-handler handlers :button-press) #'%button)
      (setf (xlib:event-handler handlers :button-release) #'%button)
      (setf (xlib:event-handler handlers :key-press) #'%key)
      (setf (event-handlers obj) handlers))))

(defmethod handle-configure-notify :before
    ((event-loop event-loop) event-key &key)
  (declare (ignore event-key))
  (let ((display (display event-loop)))
    (xlib:display-finish-output display)
  (process-gl-events display)))

(defmethod handle-destroy ((application event-loop) display window)
  (let ((glx-display (glx-display display))
	(glx-window (xlib:window-id window)))
    (XDestroyWindow glx-display glx-window)))

(defgeneric refresh (application timeout-happened))

(defgeneric run-event-loop (application &key timeout))

(defmethod run-event-loop ((application event-loop) &key (timeout .1))
  (when (or (null timeout) (> timeout .1))
    (setq timeout .1))
  (flet ((destroy-handler (display window)
	   (handle-destroy application display (xlib::lookup-window display
								    window))))
    (loop with display = (display application)
	  and handlers = (event-handlers application)
	  for timeout-happened = (null (xlib:process-event display
							   :handler handlers
							   :timeout timeout
							   :discard-p t
							   :force-output-p t))
	  do (progn
	       (process-gl-events display
				  :destroy-handler #'destroy-handler)
	       (refresh application timeout-happened)))))

(defclass basic-application (event-loop)
  ())

(defgeneric run-application (application &key host display))

(defgeneric initialize (application))

(defgeneric cleanup (application))

(define-condition application-exit (condition)
  ())

(defgeneric exit-application (application))

(defgeneric close-window (application window))
(defmethod exit-application ((application basic-application))
  (signal 'application-exit))

(defmethod run-application
    ((application basic-application) &key host (display 0))
  (setf (display application) (open-glx-display host display))
  (unwind-protect
       (handler-case
	   (progn
	     (initialize application)
	     (run-event-loop application))
	 (application-exit ()
	   nil))
    (cleanup application)
    #-(and)
    (close-gl-display (display application)))
  application)

(defmethod ensure-gl-extension ((app basic-application) extension-name
				&optional (errorp t) (error-value nil))
  (ensure-gl-extension (display app) extension-name errorp error-value))
