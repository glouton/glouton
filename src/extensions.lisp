;;; -*- Mode: Lisp -*-

;;; Copyright (c) 2005,2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :glouton)

;;; Extension mechanism

(defun make-extensions-list (str)
  (let ((*package* (find-package :keyword)))
    (loop with start = 0
	  and end-val = (cons nil nil)
	  and sym = nil
	  do (setf (values sym start) (read-from-string str nil end-val
							:start start))
	  until (eq sym end-val)
	  collect sym)))

(defclass extension ()
  ((name :accessor name :initarg :name)
   (foreign-symbol-alist :accessor foreign-symbol-alist
    :initarg :foreign-symbol-alist
    :documentation "alist of (symbol-name . variable-to-set")
   (initialized :accessor initialized :initform nil)))

(defvar *extension-table* (make-hash-table))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-extension-forms (function-defs)
    (let ((symbol-alist (mapcar #'(lambda (func-def)
				    (cons (caar func-def)
					  (intern (format nil "%~A-PTR"
							  (caar func-def)))))
				function-defs)))
      (loop
       for (nil . fn-var) in symbol-alist
       for ((foreign-name name) ret-type . arg-types) in function-defs
       for args = (mapcar #'(lambda (foo)
			      (declare (ignore foo))
			      (gensym))
			  arg-types)
       collect `(defvar ,fn-var) into defvar-forms
       append `((declaim (inline ,name))
		(defun ,name ,args
		  (cffi:foreign-funcall ,fn-var
					,@(mapcan #'list arg-types args)
					,ret-type)))
       into function-forms
       finally (return (values symbol-alist defvar-forms function-forms))))))

(defmacro define-extension (extension-name &rest functions)
  (multiple-value-bind (symbol-alist defvar-forms function-forms)
      (make-extension-forms functions)
    `(progn
      ,@defvar-forms
      ,@function-forms
      (setf (gethash ,extension-name *extension-table*)
       (make-instance
	'extension
	:name ,extension-name
	:foreign-symbol-alist ',symbol-alist)))))

(define-condition unsupported-extension (error)
  ((extension :reader extension :initarg :extension))
  (:default-initargs :extension nil))

(define-condition unsupported-server-extension (unsupported-extension)
  ()
  (:report (lambda (condition stream)
	     (format stream
		     "Extension ~S not supported in server"
		     (extension condition)))))

(define-condition unsupported-client-extension (unsupported-extension)
  ((undefined-function-name
    :reader undefined-function-name
    :initarg :undefined-function-name
    :documentation "If non-nil, the name of the first library function found that is not defined."))
  (:default-initargs :undefined-function-name nil)
  (:report
   (lambda (condition stream)
     (format stream
	     "extension ~S not supported~@[; library function ~A undefined~]"
	     (extension condition)
	     (undefined-function-name condition)))))


(defun ensure-extension-from-list (extension-name extensions
				   &optional (errorp t) (error-value nil))
  (flet ((maybe-error (&rest args)
	   (if errorp
	       (apply #'error args)
	       (return-from ensure-extension-from-list error-value))))
    (unless (member extension-name extensions :test #'eq)
      (maybe-error 'unsupported-server-extension :extension extension-name))
    (let ((extension (gethash extension-name *extension-table*)))
      (unless extension
	(maybe-error 'unsupported-client-extension :extension extension-name))
      (loop for (name . var) in (foreign-symbol-alist extension)
	    do (with-foreign-string (fn-name name)
		 (let ((address (glXGetProcAddressARB fn-name)))
		   (if (null-pointer-p address)
		       (maybe-error 'unsupported-client-extension
				    :extension extension-name
				    :undefined-function-name name)
		       (setf (symbol-value var) address)))))))
  t)
