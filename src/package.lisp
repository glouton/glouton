;;; Copyright (c) 2005 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :cl-user)

(defpackage "GLOUTON"
  (:use :common-lisp :cffi :glouton-x :glouton-gl :glouton-glx)
  (:export
   ;; Extensions
   #:ensure-gl-extension
   ;; programmatic interface
   #:clib-display
   #:mixed-display
   #:glx-display
   #:open-glx-display
   #:gl-window
   #:open-gl-window
   #:destroy-gl-window
   #:close-gl-display
   #:swap-buffers
   #:process-gl-events
   ;; application framework
   #:handle-exposure
   #:handle-configure-notify
   #:handle-button
   #:handle-key
   #:handle-destroy
   #:event-loop
   #:refresh
   #:run-event-loop
   #:basic-application
   #:run-event-loop
   #:initialize
   #:cleanup
   #:exit-application
   #:close-window
   #:run-application
   #:open-window
   #:close-window
   #:single-window-application
   #:app-window
   #:redisplayp
   ;; shapes
   #:render))



