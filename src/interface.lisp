;;; -*- Mode: Lisp -*-

;;; Copyright (c) 2005,2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package :glouton)

(defgeneric glx-display (display)
  (:documentation "Returns the C library Display object for this connection."))

(defclass clib-display ()
  ((glx-display :accessor glx-display :initarg :glx-display :initform nil
		:documentation "The C Display structure for this connection")
   (delete-atom :accessor delete-atom :initform nil
		:documentation "interned WM_DELETE_WINDOW")
   (gl-extensions :accessor gl-extensions :initform nil
		  :documentation "list of OpenGL extensions from server.")
   (glx-extensions :accessor glx-extensions :initform nil
		  :documentation "list of GLX extensions from server.")
   (glx-client-extensions
    :accessor glx-extensions :initform nil
    :documentation "list of GLX extensions from client."))
  (:documentation "Class for connection to X via the C XLib and GLX
  libraries"))

(defmethod glx-display-object ((display xlib:display))
  (getf (xlib:display-plist display) 'glx-display))

(defmethod glx-display ((display xlib:display))
  (glx-display (glx-display-object display)))

(defclass mixed-display (clib-display)
  ((x-display :accessor x-display :initarg :x-display :initform nil
	      :documentation "CLX display object"))
  (:documentation "Class for hybrid connection to X using C libraries and CLX"))

(defun graft-glx-display (x-display &optional host (display 0))
  (let ((glx-display (if host
			 (cffi:with-foreign-string
			     (display-name (format nil "~a:~d" host display))
			   (XOpenDisplay display-name))
			 (XOpenDisplay (cffi:null-pointer)))))
    (setf (getf (xlib:display-plist x-display) 'glx-display)
	  (make-instance 'mixed-display
			 :x-display x-display :glx-display glx-display))
    x-display))


(defun open-glx-display (&optional host (display 0))
  "Open a CLX display connection and a C XLib connection suitable for GL
  operations. Returns a CLX display object that can be used by Glouton
  functions."
  (let ((x-display (if host
		       (xlib:open-display host :display display)
		       (xlib:open-default-display))))
    (graft-glx-display x-display host display)))

(defclass gl-context ()
  ((glx-context :accessor glx-context :initarg :glx-context :initform nil
		:documentation "glX graphics context")
   (extensions :accessor extensions :initarg :extensions :initform nil)
   (glx-visual :accessor glx-visual :initarg :glx-visual :initform nil))
  (:documentation "Wrapper around glX / OpenGL graphics context"))

(defgeneric initialize-extensions (context extensions-string))

(defmethod initialize-extensions ((context gl-context) extensions-string)
  (setf (extensions context) (make-extensions-list extensions-string)))

(defmethod initialize-instance :after ((obj gl-context) &key extensions-string)
  (when extensions-string
    (initialize-extensions obj extensions-string)))

(defclass gl-window ()
  ((display :accessor display :initarg :display :initform nil)
   (window :accessor window :initarg :window :initform nil
	   :documentation "The CLX window object")
   (visual :accessor visual :initarg :visual :initform nil
	   :documentation "The CLX visual-info object")
   (context :accessor context :initarg :context :initform nil)))



(defun depth-of-visual (visual)
  (loop
     for screen in (xlib:display-roots (xlib:visual-info-display visual))
     do (loop
	   for (depth . visuals) in (xlib:screen-depths screen)
	   if (member visual visuals)
	     do (return-from depth-of-visual depth)
	   end))
  (error "No depth found for visual ~S" visual))

(defun create-visual-and-context (display visual-attributes)
  "Returns a CLX visual-info and OpenGL context for given visual attributes.

   DISPLAY - the CLX display object
   VISUAL-ATTRIBUTES - a list of GLX constants representing the desired visual,
   terminated by GLOUTON-X:None.

   returns CLX visual-info object and gl-context object"
  (let ((glx-display (glx-display display))
	(screen-number 0))		;XXX
    (cffi:with-foreign-object (attrs :int (length visual-attributes))
      (loop
	 for attr in visual-attributes
	 for i from 0
	 do (setf (cffi:mem-aref attrs :int i) attr))
      (let ((visinfo (glXChooseVisual glx-display screen-number attrs)))
	(when (cffi:null-pointer-p visinfo)
	  (error "Couldn't get requested visual"))
	(let* ((x-visual (xlib:visual-info
			  display
			  (cffi:foreign-slot-value
			   visinfo 'XVisualInfo 'visualID)))
	       (glx-context (glXCreateContext glx-display
					      visinfo
					      (cffi:null-pointer)
					      1)))
	  (values x-visual
		  (make-instance 'gl-context
				 :glx-context glx-context
				 :glx-visual visinfo)
		  visinfo))))))

(defun make-current (display drawable context)
  "Makes the Glouton context object CONTEXT currrent with CLX DRAWABLE"
  (xlib:display-finish-output display)
  (let ((result (glxMakeCurrent (glx-display display)
				(xlib:drawable-id drawable)
				(glx-context context))))
    (when (= result 0)
      (return-from make-current nil))
    (glFinish)
    t))

(defun open-gl-window (display width height event-mask
		       &key parent visual-attributes visual context
		       (wm-delete-window t))
  "Convenience routine for opening one X window for GL rendering"
  (unless (or visual-attributes (and visual context))
    (error "Must specify VISUAL-ATTRIBUTES or VISUAL and CONTEXT"))
  (multiple-value-bind (x-visual context glx-visual)
      (if visual-attributes
	  (create-visual-and-context display visual-attributes)
	  (values visual context (glx-visual context)))
    (let* ((x-screen (xlib:display-default-screen display))
	   (x-root (xlib:screen-root x-screen))
	   (glx-root (xlib:window-id x-root))
	   (glx-display (glx-display display))
	   (glx-parent (if parent
			   (xlib:window-id parent)
			   glx-root))
	   (visual-id (xlib:visual-info-id x-visual)))
      (with-foreign-object (attributes 'XSetWindowAttributes)
	(macrolet ((attr-slot-value (attr slot)
		     `(foreign-slot-value ,attr 'XSetWindowAttributes ,slot)))
	  (setf (attr-slot-value attributes 'glouton-x::border_pixel) 0)
	  (setf (attr-slot-value attributes 'glouton-x::colormap)
		(XCreateColormap glx-display
				 glx-root
				 glx-visual
				 AllocNone)))
	(let ((glx-window (XCreateWindow glx-display
					 glx-parent
					 0
					 0
					 width
					 height
					 0
					 (foreign-slot-value glx-visual
							     'XVisualInfo
							     'glouton-x::depth)
					 InputOutput
					 glx-visual
					 (logior CWBorderPixel CWColormap)
					 attributes))
	      (window nil))
	  (when wm-delete-window
	    (with-accessors ((delete-atom delete-atom))
	      (glx-display-object display)
	      (setf delete-atom (with-foreign-string (arg "WM_DELETE_WINDOW")
				  (XInternAtom glx-display arg False)))
	      (with-foreign-object (atoms 'glouton-x::Atom)
		(setf (mem-aref atoms 'glouton-x::Atom 0) delete-atom)
		(XSetWMProtocols glx-display glx-window atoms 1))))
	  
	  (XSync glx-display True)
	  (glxMakeCurrent glx-display glx-window (glx-context context))
	  (glFinish)
	  (process-gl-events display)
	  (initialize-extensions context (cffi:foreign-string-to-lisp
					  (glGetString GL_EXTENSIONS)))
	  (setq window (xlib::lookup-window display glx-window))
	  (setf (xlib:window-event-mask window)
		(xlib::encode-event-mask event-mask))
	  (xlib:map-window window)
	  (xlib:display-finish-output display)
	  (process-gl-events display)
	  (setf (getf (xlib:window-plist window) 'glx-window)
		(make-instance 'gl-window
			       :display display
			       :window window
			       :visual x-visual
			       :context context))
	  window)))))

(defun glx-window (window)
  (getf (xlib:window-plist window) 'glx-window))

(defun destroy-gl-window (window &optional (destroy-context t))
  (let* ((display (xlib:window-display window))
	 (glx-display (glx-display display))
	 (window-id (xlib:window-id window)))
    (when destroy-context
      (process-gl-events display :ignore t)
      (glXDestroyContext glx-display
			 (glx-context (context (glx-window window)))))
    (XDestroyWindow glx-display window-id)
    (process-gl-events display)))

(defun close-gl-display (display)
  (process-gl-events display)
  (XCloseDisplay (glx-display display))
  (xlib:close-display display))

(defun swap-buffers (window)
  (let ((display (xlib:window-display window)))
    (xlib:display-finish-output display)
    (process-gl-events display)
    (glXSwapBuffers (glx-display display) (xlib:window-id window))
    (glFlush)))

(defun report-gl-error ()
  (let ((errno (glGetError)))
    (unless (eql errno GL_NO_ERROR)
      (let* ((err-cstring (gluErrorString errno))
	     (errstring (cffi:foreign-string-to-lisp err-cstring)))
	(format *debug-io* "~A~%" errstring)))))

(defun process-gl-events (display &key destroy-handler ignore)
  (let ((glx-display (glx-display display))
	(delete-atom (delete-atom (glx-display-object display))))
    (cffi:with-foreign-object (event 'XEvent)
      (loop
	 until (zerop (XPending glx-display))
	 do (progn
	      (XNextEvent glx-display event)
	      (unless ignore
		(let ((event-type (foreign-slot-value event
						      'XEvent
						      'glouton-x::type)))
		  (cond ((eql event-type ClientMessage)
			 (let* ((client-event (foreign-slot-value
					       event
					       'XEvent
					       'glouton-x::xclient))
				(window (foreign-slot-value
					 client-event
					 'XClientMessageEvent
					 'glouton-x::window))
				(message (mem-aref
					  (foreign-slot-value
					   (foreign-slot-value
					    client-event
					    'XClientMessageEvent
					    'glouton-x::data)
					   'glouton-x::XClientMessageEvent-data
					   'glouton-x::l)
					  :long)))
			   (if (eql message delete-atom)
			       (if destroy-handler
				   (funcall destroy-handler display window)
				   ;; XXX What about the context?
				   (XDestroyWindow glx-display window))
			       (format *debug-io*
				       "message =~s delete-atom = ~s~%"
				       message delete-atom)))))))
	      (format *debug-io* "Got event ~d~%"
		      (foreign-slot-value event
					  'XEvent
					  'glouton-x::type)))))))

(defgeneric ensure-gl-extension
    (display extension-name &optional errorp error-value)
  (:documentation "Ensure that the OpenGL extension named by the keyword
EXTENSION-NAME is supported on the client and server. Validate all the
functions in the extension's API.

Returns T if the extension is supported. If it is not supported and ERRORP is T
(the default), a condition of type UNSUPPORTED-EXTENSION is signalled;
otherwise, ERROR-VALUE is returned."))

(defmethod ensure-gl-extension ((display clib-display) extension-name
				&optional (errorp t) (error-value nil))
  (with-accessors ((gl-extensions gl-extensions))
    display
    (when (null gl-extensions)
      (setf gl-extensions
	    (make-extensions-list
	     (foreign-string-to-lisp (glGetString GL_EXTENSIONS)))))
    (ensure-extension-from-list extension-name
				gl-extensions
				errorp
				error-value)))

(defmethod ensure-gl-extension ((display xlib:display) extension-name
				&optional (errorp t) (error-value nil))
  (ensure-gl-extension (glx-display-object display)
		       extension-name
		       errorp
		       error-value))
