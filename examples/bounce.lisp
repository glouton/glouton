;;;
;;; BOUNCE.LISP
;;; This example came from Knut Arild Erstad's bindings for CMUCL / SBCL,
;;; where it bears this comment:
;;;
;;; Translation of "bounce.c" by Brian Paul
;;; Changed to use RGB mode.
;;;
;;; Modified slightly by tbmoore for glouton. The original C program is in
;;; the public domain, so I assume this file is too.

(in-package :glouton-examples)

(defun sind (x)
  (coerce (sin (* x (/ pi 180.))) 'single-float))

(defun cosd (x)
  (coerce (cos (* x (/ pi 180.))) 'single-float))

(defvar ball)
(defvar window)
(defvar Zrot)
(defvar Zstep)
(defvar Xpos)
(defvar Ypos)
(defvar Xvel)
(defvar Yvel)
(defvar Xmin)
(defvar Xmax)
(defvar Ymin)
(defvar Ymax)
(defvar G)
(defvar vel0)

(defvar *animate?*)
(defvar *display* nil)
(defvar *window*)
(defvar *debug* nil)

(defvar *time* 0 "Time since beginning of animation, in internal time units")
(defvar *time-units* (float internal-time-units-per-second))
(defparameter *nominal-frame-interval* 0.005 "Frame rate assumed for velocity and
acceleration")

(setq Zrot 0.0
      Zstep 0.6
      Xpos 0.0 Ypos 1.0
      Xvel 0.02 Yvel 0.0
      Xmin -4.0 Xmax 4.0
      Ymin -3.8 Ymax 4.0
      G -0.001
      vel0 -100.0)

(defun make-ball ()
  (let ((da 18.0) (db 18.0)
	(radius 1.0)
	(color 0)
	(x 0.0) (y 0.0) (z 0.0))
    
    (setq ball (glGenLists 1))
    (glNewList ball GL_COMPILE)
    
    (setq color 0)
    (do ((a -90.0 (+ a da))) ((> (+ a da) 90.0))
      ;;
      (glBegin GL_QUAD_STRIP)
      (do ((b 0.0 (+ b db))) ((> b 360.0))
	
	; (format t "b:~a, color:~a~%" b color)
	(if (> color 0)
	  (glColor3f 1.0 0.0 0.0)
	  (glColor3f 1.0 1.0 1.0))
	
	(setq x (* (cosd b) (cosd a)))
	(setq y (* (sind b) (cosd a)))
	(setq z (sind a))
	(glVertex3f x y z)
	
	(setq x (* radius (cosd b) (cosd (+ a da))))
	(setq y (* radius (sind b) (cosd (+ a da))))
	(setq z (* radius (sind (+ a da))))
	(glVertex3f x y z)
	
	(setq color (- 1 color)))
      
      (glEnd))
    
    (glEndList)))

(defun reshape (width height)
  (when *debug* (format t "RESHAPE. WIDTH:~a, HEIGHT:~a~%" width height))
  (glViewport 0 0 width height)
  (glMatrixMode GL_PROJECTION)
  (glLoadIdentity)
  (glOrtho -6d0 6d0 -6d0 6d0 -6d0 6d0)
  (glMatrixMode GL_MODELVIEW))

(defun idle ()
  (let* ((current-time (get-internal-real-time))
	 (frame-interval (/ (- current-time *time*) *time-units*))
	 (frame-scale (/ frame-interval *nominal-frame-interval*)))
    (setq *time* current-time)
    (when *debug* (format t "Callback IDLE.~%"))
    (setq zrot (+ zrot (* zstep frame-scale)))
  
    (setq xpos (+ xpos (* xvel frame-scale)))
    (when (>= xpos xmax)
      (setq xpos xmax)
      (setq xvel (- xvel))
      (setq zstep (- zstep)))
    ;;
    (when (<= xpos xmin)
      (setq xpos xmin)
      (setq xvel (- xvel))
      (setq zstep (- zstep)))
    ;;
    (setq ypos (+ ypos (* yvel frame-scale)))
    (setq yvel (+ yvel (* g frame-scale)))
    (when (< ypos ymin)
      (setq ypos ymin)
      (when (= vel0 -100.0) (setq vel0 (abs yvel)))
      (setq yvel vel0))
    (draw)))

(defun draw ()
  (when *debug* (format t "Callback DRAW.~%"))
  (glClear GL_COLOR_BUFFER_BIT)
  (glColor3f 0.0 1.0 1.0)
  (glBegin GL_LINES)
  (do ((i -5 (+ i 1))) ((> i 5))
    (glVertex2i i -5) (glVertex2i i 5))
  ;;
  (do ((i -5 (+ i 1))) ((> i 5))
    (glVertex2i -5 i) (glVertex2i 5 i))
  ;;
  (do ((i -5 (+ i 1))) ((> i 5))
    (glVertex2i i -5) (glVertex2f (* i 1.15) -5.9))
  ;;
  (glVertex2f -5.3 -5.35)  (glVertex2f 5.3 -5.35)
  (glVertex2f -5.75 -5.9)  (glVertex2f 5.75 -5.9)
  (glEnd)
  
  (glPushMatrix)
  (glTranslatef Xpos Ypos 0.0)
  (glScalef 2.0 2.0 2.0)
  (glRotatef 8.0 0.0 0.0 1.0)
  (glRotatef 90.0 1.0 0.0 0.0)
  (glRotatef Zrot 0.0 0.0 1.0)
  
  (glCallList ball)
  (glPopMatrix)
  (glFlush)
  (when *debug* (format t "swapbuffers~%"))
  (swap-buffers *window*)
  (when *debug* (format t "exit DRAW~%")))

(defun event-loop (display)
  (let ((done? nil)
	(debug nil))
    (loop
       (when *animate?*
	 (when debug (format t "Animate...~%"))
	 (idle))
       (event-case (display :timeout (if *animate?*
					 0
					 nil)
			      :force-output-p t)
	 (:exposure (count)
		    (when (zerop count)
		      (draw))
		    (format *debug-io* "exposure~%" )
		    t)
	 (:configure-notify (width height)
			    (format *debug-io* "configure ~S ~S~%" width height)
			    (xlib:display-finish-output display)
			    (clear-gl-events display)
			    (reshape width height)
			    t)
	 (:button-press (code)
			(format *debug-io* "button press ~S~%" code)
			(cond ((eql code 1)
			       (setf *animate?* (not *animate?*))
			       (setf *time* (get-internal-real-time)))
			      ((eql code 3)
			       (setf done? t))
			      (t nil))
			t)
	 (otherwise () t))
       (glouton::report-gl-error)
       (when done?
	 (return)))))

(defparameter +glx-attributes+ (list GLX_RGBA GLX_RED_SIZE 1
				     GLX_GREEN_SIZE 1 GLX_BLUE_SIZE 1
				     GLX_DOUBLEBUFFER glouton-x:None))

(defun bounce ()
    ;;(setq *display* (xopendisplay ""))
  (let ((*display* (open-glx-display))
	(*window* nil))
    (unwind-protect
	 (progn
	   (setq *window*
		 (open-gl-window *display* 300 300
				 '(:structure-notify :exposure :button-press)
				 :visual-attributes +glx-attributes+))
	   ;; Create display list for ball
	   (make-ball)
	   ;; some GL init
	   (glcullface GL_BACK)
	   (glenable GL_CULL_FACE)
	   (gldisable GL_DITHER)
	   (glshademodel GL_FLAT)
	   ;; Initial state of animation.
	   (setf *animate?* t)
	   (setq *time* (get-internal-real-time))
	   ;; start event loop
	   (event-loop *display*))
      (when *window*
	(destroy-gl-window *window*))
      (close-gl-display *display*))))


