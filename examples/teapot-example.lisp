;;; Copyright (c) 2005,2006 by Tim Moore (moore@bricoworks.com)

;;; Permission is hereby granted, free of charge, to any person obtaining a
;;; copy of this software and associated documentation files (the "Software"),
;;; to deal in the Software without restriction, including without limitation
;;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;; and/or sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

(in-package cl-user)

(in-package :glouton-examples)

(defclass teapot-example (single-window-application)
  ((light-pos :accessor light-pos :initarg :light-pos)
   (light-color :accessor light-color :initarg :light-color)
   (teapot :accessor teapot :initform nil))
  (:default-initargs
    :light-pos (cffi:foreign-alloc :float :initial-contents '(0.0 0.0 1.0 0.0))
    :light-color (cffi:foreign-alloc :float :initial-contents '(1.0 1.0 1.0 1.0))))

(defmethod initialize :after ((application teapot-example))
  (setf (teapot application) (make-instance 'glouton::teapot))
  (glCullFace GL_BACK)
  (glEnable GL_CULL_FACE)
  (glEnable GL_DEPTH_TEST)
  (glDepthFunc GL_LESS)
  (glDisable GL_DITHER)
  (glShadeModel GL_SMOOTH)
  (glLightModeli GL_LIGHT_MODEL_LOCAL_VIEWER 1)
  (glEnable GL_LIGHT0)
  (glEnable GL_LIGHTING)
  (glColorMaterial GL_FRONT GL_AMBIENT_AND_DIFFUSE)
  ;; Test of extension mechanism
  (if (ensure-gl-extension application :GL_ARB_multitexture nil)
      (format t "multitexturing extension supported~%")
      (format t "multitexturing extension not supported~%"))
  (format t "OpenGL version: ~s~%"
	  (cffi:foreign-string-to-lisp (glGetString GL_VERSION))))

(defmethod handle-configure-notify
    ((application teapot-example) event-key &key width height)
  (declare (ignore event-key))
  (glViewport 0 0 width height)
  (glMatrixMode GL_PROJECTION)
  (glLoadIdentity)
  (gluPerspective 50.0d0 (float (/ width height) 1.0d0) .5d0 20.0d0)
  (glMatrixMode GL_MODELVIEW)
  (glLoadIdentity))

(defmethod refresh ((application teapot-example) timeout-happened)
  (declare (ignore timeout-happened))
  (when (redisplayp application)
    (glLoadIdentity)
    (glTranslatef 0.0 0.0 -5.0)
    (glLightfv GL_LIGHT0 GL_POSITION (light-pos application))
    (glLightfv GL_LIGHT0 GL_DIFFUSE (light-color application))
    (glClear (logior GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT))
    (glColor3f 1.0 1.0 1.0)
    (render (teapot application) 10 1 GL_FILL)
    (swap-buffers (app-window application))))

(defmethod handle-button
    ((application teapot-example) (event-key (eql :button-press)) &key code)
  (when (eql code 3)
    (exit-application application)))
