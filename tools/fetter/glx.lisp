(defpackage "GLOUTON-GLX" (:use #:CFFI) (:nicknames)
 (:export "_XDISPLAY" "__GLXFBCONFIGREC" "VISUAL" "__GLXCONTEXTREC" "_XEXTDATA"
  "XVISUALINFO" "GLXCONTEXTID" "PFNGLXFREEMEMORYNVPROC" "GLFLOAT"
  "PFNGLXALLOCATEMEMORYNVPROC" "GLXFBCONFIG" "GLXFBCONFIGID" "PIXMAP"
  "XEXTDATA" "WINDOW" "GLXWINDOW" "GLXPBUFFER" "GLXPIXMAP" "XID" "FONT"
  "GLXCONTEXT" "GLUBYTE" "GLXDRAWABLE" "DISPLAY" "VISUALID" "GLSIZEI"
  "XPOINTER" "GLVOID" "GLXDESTROYPBUFFER" "GLXWAITGL" "GLXBINDTEXIMAGEARB"
  "GLXCREATEGLXPIXMAP" "GLXGETCURRENTDISPLAY" "GLXGETCLIENTSTRING"
  "GLXCREATEPIXMAP" "GLXSWAPBUFFERS" "GLXGETVISUALFROMFBCONFIG"
  "GLXGETCURRENTDRAWABLE" "GLXWAITX" "GLXGETSELECTEDEVENT" "GLXDESTROYPIXMAP"
  "GLXCREATEWINDOW" "GLXGETCURRENTREADDRAWABLE" "GLXCHOOSEVISUAL"
  "GLXMAKECONTEXTCURRENT" "GLXDESTROYCONTEXT" "GLXSELECTEVENT"
  "GLXFREEMEMORYNV" "GLXCREATEPBUFFER" "GLXCOPYCONTEXT" "GLXQUERYDRAWABLE"
  "GLXCHOOSEFBCONFIG" "GLXUSEXFONT" "GLXQUERYEXTENSIONSSTRING"
  "GLXDESTROYGLXPIXMAP" "GLXQUERYCONTEXT" "GLXCREATECONTEXT"
  "GLXQUERYSERVERSTRING" "GLXGETFBCONFIGS" "GLXDRAWABLEATTRIBARB"
  "GLXGETPROCADDRESS" "GLXMAKECURRENT" "GLXISDIRECT" "GLXDESTROYWINDOW"
  "GLXCREATENEWCONTEXT" "GLXQUERYVERSION" "GLXRELEASETEXIMAGEARB"
  "GLXGETCURRENTCONTEXT" "GLXQUERYEXTENSION" "GLXALLOCATEMEMORYNV"
  "GLXGETFBCONFIGATTRIB" "GLXGETCONFIG" "GLX_PIXMAP_BIT" "GLX_BAD_VISUAL"
  "GLX_X_VISUAL_TYPE" "GLX_BLUE_SIZE" "GLX_COLOR_INDEX_BIT" "GLX_WINDOW_BIT"
  "GLX_ACCUM_RED_SIZE" "GLX_BAD_CONTEXT" "GLX_DRAWABLE_TYPE" "GLX_AUX_BUFFERS"
  "GLX_ACCUM_BUFFER_BIT" "GLX_STEREO" "GLX_SLOW_CONFIG" "GLX_DEPTH_BUFFER_BIT"
  "GLX_MAX_PBUFFER_PIXELS" "GLX_TRANSPARENT_RED_VALUE" "GLX_CONFIG_CAVEAT"
  "GLX_HEIGHT" "GLX_STATIC_GRAY" "GLX_STENCIL_BUFFER_BIT" "GLX_X_RENDERABLE"
  "GLX_FRONT_LEFT_BUFFER_BIT" "GLX_TRANSPARENT_ALPHA_VALUE"
  "GLX_AUX_BUFFERS_BIT" "GLX_LARGEST_PBUFFER" "GLX_RENDER_TYPE"
  "GLX_GREEN_SIZE" "GLX_TRUE_COLOR" "GLX_EXTENSIONS" "GLX_SAVED"
  "GLX_MAX_PBUFFER_HEIGHT" "GLX_PRESERVED_CONTENTS" "GLX_BAD_SCREEN"
  "GLX_GRAY_SCALE" "GLX_ACCUM_ALPHA_SIZE" "GLX_ACCUM_GREEN_SIZE" "GLX_WINDOW"
  "GLX_BAD_ATTRIBUTE" "GLX_FRONT_RIGHT_BUFFER_BIT" "GLX_RGBA_BIT" "GLX_USE_GL"
  "GLX_PBUFFER_CLOBBER_MASK" "GLX_PSEUDO_COLOR" "GLX_VERSION_1_1"
  "GLX_BUFFER_SIZE" "GLX_TRANSPARENT_INDEX" "GLX_DIRECT_COLOR"
  "GLX_VERSION_1_3" "GLX_TRANSPARENT_BLUE_VALUE" "GLX_STATIC_COLOR"
  "GLX_VENDOR" "GLX_SCREEN" "GLX_NON_CONFORMANT_CONFIG" "GLX_STENCIL_SIZE"
  "GLX_BACK_LEFT_BUFFER_BIT" "GLX_DAMAGED" "GLX_FBCONFIG_ID" "GLX_VERSION"
  "GLX_BACK_RIGHT_BUFFER_BIT" "GLX_COLOR_INDEX_TYPE" "GLX_NONE" "GLX_RGBA"
  "GLX_TRANSPARENT_INDEX_VALUE" "GLX_DEPTH_SIZE" "GLX_SAMPLES"
  "GLX_TRANSPARENT_TYPE" "GLX_TRANSPARENT_GREEN_VALUE" "GLX_MAX_PBUFFER_WIDTH"
  "GLX_ALPHA_SIZE" "GLX_PBUFFER_BIT" "GLX_RED_SIZE" "GLX_ARB_RENDER_TEXTURE"
  "GLX_DONT_CARE" "GLX_PBUFFER" "GLX_BAD_ENUM" "GLX_WIDTH" "GLX_PBUFFER_HEIGHT"
  "GLX_TRANSPARENT_RGB" "GLX_ACCUM_BLUE_SIZE" "GLX_DOUBLEBUFFER"
  "GLX_EVENT_MASK" "GLX_VERSION_1_2" "GLX_BAD_VALUE" "GLX_NO_EXTENSION"
  "GLX_RGBA_TYPE" "GLX_SAMPLE_BUFFERS" "GLX_VISUAL_ID" "GLX_PBUFFER_WIDTH"
  "GLX_LEVEL" "GLX_VERSION_1_4"))

(in-package "GLOUTON-GLX")
(asdf:operate 'asdf:load-op 'verrazano-support)
(cffi:define-foreign-type xpointer () ':pointer)
(cffi:defcstruct _xextdata (number :int) (next :pointer)
 (free_private :pointer) (private_data xpointer))
(cffi:define-foreign-type xextdata () '_xextdata)
(cffi:define-foreign-type visualid () ':unsigned-long)
(cffi:defcstruct visual (ext_data :pointer) (visualid visualid) (c_class :int)
 (red_mask :unsigned-long) (green_mask :unsigned-long)
 (blue_mask :unsigned-long) (bits_per_rgb :int) (map_entries :int))
(cffi:defcstruct xvisualinfo (visual :pointer) (visualid visualid)
 (screen :int) (depth :int) (c_class :int) (red_mask :unsigned-long)
 (green_mask :unsigned-long) (blue_mask :unsigned-long) (colormap_size :int)
 (bits_per_rgb :int))
(cffi:define-foreign-type display () '_xdisplay)
(cffi:defcstruct _xdisplay)
(cffi:define-foreign-type glxdrawable () 'xid)
(cffi:define-foreign-type xid () ':unsigned-long)
(cffi:define-foreign-type glxpbuffer () 'xid)
(cffi:define-foreign-type pfnglxfreememorynvproc () ':pointer)
(cffi:define-foreign-type glvoid () ':void)
(cffi:define-foreign-type pfnglxallocatememorynvproc () ':pointer)
(cffi:define-foreign-type glsizei () ':int)
(cffi:define-foreign-type glfloat () ':float)
(cffi:define-foreign-type glubyte () ':unsigned-char)
(cffi:define-foreign-type glxcontext () ':pointer)
(cffi:defcstruct __glxcontextrec)
(cffi:define-foreign-type glxfbconfig () ':pointer)
(cffi:defcstruct __glxfbconfigrec)
(cffi:define-foreign-type glxpixmap () 'xid)
(cffi:define-foreign-type pixmap () 'xid)
(cffi:define-foreign-type glxwindow () 'xid)
(cffi:define-foreign-type window () 'xid)
(cffi:define-foreign-type font () 'xid)
(cffi:define-foreign-type glxcontextid () 'xid)
(cffi:define-foreign-type glxfbconfigid () 'xid)
(cl:progn
 (cffi:defcfun ("glXDrawableAttribARB" glxdrawableattribarb) :int
  (dpy :pointer) (draw glxdrawable) (attriblist :pointer))
 (cffi:defcfun ("glXReleaseTexImageARB" glxreleaseteximagearb) :int
  (dpy :pointer) (pbuffer glxpbuffer) (buffer :int))
 (cffi:defcfun ("glXBindTexImageARB" glxbindteximagearb) :int (dpy :pointer)
  (pbuffer glxpbuffer) (buffer :int))
 (cffi:defcfun ("glXFreeMemoryNV" glxfreememorynv) :void (pointer :pointer))
 (cffi:defcfun ("glXAllocateMemoryNV" glxallocatememorynv) :pointer
  (size glsizei) (readfreq glfloat) (writefreq glfloat) (priority glfloat))
 (cffi:defcfun ("glXGetProcAddress" glxgetprocaddress) :pointer
  (procname :pointer))
 (cffi:defcfun ("glXGetSelectedEvent" glxgetselectedevent) :void (dpy :pointer)
  (drawable glxdrawable) (mask :pointer))
 (cffi:defcfun ("glXSelectEvent" glxselectevent) :void (dpy :pointer)
  (drawable glxdrawable) (mask :unsigned-long))
 (cffi:defcfun ("glXQueryContext" glxquerycontext) :int (dpy :pointer)
  (ctx glxcontext) (attribute :int) (value :pointer))
 (cffi:defcfun ("glXGetCurrentReadDrawable" glxgetcurrentreaddrawable)
  glxdrawable)
 (cffi:defcfun ("glXMakeContextCurrent" glxmakecontextcurrent) :int
  (dpy :pointer) (draw glxdrawable) (read glxdrawable) (ctx glxcontext))
 (cffi:defcfun ("glXCreateNewContext" glxcreatenewcontext) glxcontext
  (dpy :pointer) (config glxfbconfig) (rendertype :int) (sharelist glxcontext)
  (direct :int))
 (cffi:defcfun ("glXQueryDrawable" glxquerydrawable) :void (dpy :pointer)
  (draw glxdrawable) (attribute :int) (value :pointer))
 (cffi:defcfun ("glXDestroyPbuffer" glxdestroypbuffer) :void (dpy :pointer)
  (pbuf glxpbuffer))
 (cffi:defcfun ("glXCreatePbuffer" glxcreatepbuffer) glxpbuffer (dpy :pointer)
  (config glxfbconfig) (attriblist :pointer))
 (cffi:defcfun ("glXDestroyPixmap" glxdestroypixmap) :void (dpy :pointer)
  (pixmap glxpixmap))
 (cffi:defcfun ("glXCreatePixmap" glxcreatepixmap) glxpixmap (dpy :pointer)
  (config glxfbconfig) (pixmap pixmap) (attriblist :pointer))
 (cffi:defcfun ("glXDestroyWindow" glxdestroywindow) :void (dpy :pointer)
  (window glxwindow))
 (cffi:defcfun ("glXCreateWindow" glxcreatewindow) glxwindow (dpy :pointer)
  (config glxfbconfig) (win window) (attriblist :pointer))
 (cffi:defcfun ("glXGetVisualFromFBConfig" glxgetvisualfromfbconfig) :pointer
  (dpy :pointer) (config glxfbconfig))
 (cffi:defcfun ("glXGetFBConfigs" glxgetfbconfigs) :pointer (dpy :pointer)
  (screen :int) (nelements :pointer))
 (cffi:defcfun ("glXGetFBConfigAttrib" glxgetfbconfigattrib) :int
  (dpy :pointer) (config glxfbconfig) (attribute :int) (value :pointer))
 (cffi:defcfun ("glXChooseFBConfig" glxchoosefbconfig) :pointer (dpy :pointer)
  (screen :int) (attriblist :pointer) (nitems :pointer))
 (cffi:defcfun ("glXGetCurrentDisplay" glxgetcurrentdisplay) :pointer)
 (cffi:defcfun ("glXGetClientString" glxgetclientstring) :pointer
  (dpy :pointer) (name :int))
 (cffi:defcfun ("glXQueryServerString" glxqueryserverstring) :pointer
  (dpy :pointer) (screen :int) (name :int))
 (cffi:defcfun ("glXQueryExtensionsString" glxqueryextensionsstring) :pointer
  (dpy :pointer) (screen :int))
 (cffi:defcfun ("glXUseXFont" glxusexfont) :void (font font) (first :int)
  (count :int) (list :int))
 (cffi:defcfun ("glXWaitX" glxwaitx) :void)
 (cffi:defcfun ("glXWaitGL" glxwaitgl) :void)
 (cffi:defcfun ("glXGetCurrentDrawable" glxgetcurrentdrawable) glxdrawable)
 (cffi:defcfun ("glXGetCurrentContext" glxgetcurrentcontext) glxcontext)
 (cffi:defcfun ("glXGetConfig" glxgetconfig) :int (dpy :pointer)
  (visual :pointer) (attrib :int) (value :pointer))
 (cffi:defcfun ("glXIsDirect" glxisdirect) :int (dpy :pointer)
  (ctx glxcontext))
 (cffi:defcfun ("glXQueryVersion" glxqueryversion) :int (dpy :pointer)
  (maj :pointer) (min :pointer))
 (cffi:defcfun ("glXQueryExtension" glxqueryextension) :int (dpy :pointer)
  (errorb :pointer) (event :pointer))
 (cffi:defcfun ("glXDestroyGLXPixmap" glxdestroyglxpixmap) :void (dpy :pointer)
  (pixmap glxpixmap))
 (cffi:defcfun ("glXCreateGLXPixmap" glxcreateglxpixmap) glxpixmap
  (dpy :pointer) (visual :pointer) (pixmap pixmap))
 (cffi:defcfun ("glXSwapBuffers" glxswapbuffers) :void (dpy :pointer)
  (drawable glxdrawable))
 (cffi:defcfun ("glXCopyContext" glxcopycontext) :void (dpy :pointer)
  (src glxcontext) (dst glxcontext) (mask :unsigned-long))
 (cffi:defcfun ("glXMakeCurrent" glxmakecurrent) :int (dpy :pointer)
  (drawable glxdrawable) (ctx glxcontext))
 (cffi:defcfun ("glXDestroyContext" glxdestroycontext) :void (dpy :pointer)
  (ctx glxcontext))
 (cffi:defcfun ("glXCreateContext" glxcreatecontext) glxcontext (dpy :pointer)
  (vis :pointer) (sharelist glxcontext) (direct :int))
 (cffi:defcfun ("glXChooseVisual" glxchoosevisual) :pointer (dpy :pointer)
  (screen :int) (attriblist :pointer))
 (cl:defparameter glx_version_1_1 1) (cl:defparameter glx_version_1_2 1)
 (cl:defparameter glx_version_1_3 1) (cl:defparameter glx_version_1_4 1)
 (cl:defparameter glx_use_gl 1) (cl:defparameter glx_buffer_size 2)
 (cl:defparameter glx_level 3) (cl:defparameter glx_rgba 4)
 (cl:defparameter glx_doublebuffer 5) (cl:defparameter glx_stereo 6)
 (cl:defparameter glx_aux_buffers 7) (cl:defparameter glx_red_size 8)
 (cl:defparameter glx_green_size 9) (cl:defparameter glx_blue_size 10)
 (cl:defparameter glx_alpha_size 11) (cl:defparameter glx_depth_size 12)
 (cl:defparameter glx_stencil_size 13) (cl:defparameter glx_accum_red_size 14)
 (cl:defparameter glx_accum_green_size 15)
 (cl:defparameter glx_accum_blue_size 16)
 (cl:defparameter glx_accum_alpha_size 17) (cl:defparameter glx_bad_screen 1)
 (cl:defparameter glx_bad_attribute 2) (cl:defparameter glx_no_extension 3)
 (cl:defparameter glx_bad_visual 4) (cl:defparameter glx_bad_context 5)
 (cl:defparameter glx_bad_value 6) (cl:defparameter glx_bad_enum 7)
 (cl:defparameter glx_vendor 1) (cl:defparameter glx_version 2)
 (cl:defparameter glx_extensions 3) (cl:defparameter glx_config_caveat 32)
 (cl:defparameter glx_dont_care 4294967295)
 (cl:defparameter glx_x_visual_type 34)
 (cl:defparameter glx_transparent_type 35)
 (cl:defparameter glx_transparent_index_value 36)
 (cl:defparameter glx_transparent_red_value 37)
 (cl:defparameter glx_transparent_green_value 38)
 (cl:defparameter glx_transparent_blue_value 39)
 (cl:defparameter glx_transparent_alpha_value 40)
 (cl:defparameter glx_window_bit 1) (cl:defparameter glx_pixmap_bit 2)
 (cl:defparameter glx_pbuffer_bit 4) (cl:defparameter glx_aux_buffers_bit 16)
 (cl:defparameter glx_front_left_buffer_bit 1)
 (cl:defparameter glx_front_right_buffer_bit 2)
 (cl:defparameter glx_back_left_buffer_bit 4)
 (cl:defparameter glx_back_right_buffer_bit 8)
 (cl:defparameter glx_depth_buffer_bit 32)
 (cl:defparameter glx_stencil_buffer_bit 64)
 (cl:defparameter glx_accum_buffer_bit 128) (cl:defparameter glx_none 32768)
 (cl:defparameter glx_slow_config 32769) (cl:defparameter glx_true_color 32770)
 (cl:defparameter glx_direct_color 32771)
 (cl:defparameter glx_pseudo_color 32772)
 (cl:defparameter glx_static_color 32773)
 (cl:defparameter glx_gray_scale 32774) (cl:defparameter glx_static_gray 32775)
 (cl:defparameter glx_transparent_rgb 32776)
 (cl:defparameter glx_transparent_index 32777)
 (cl:defparameter glx_visual_id 32779) (cl:defparameter glx_screen 32780)
 (cl:defparameter glx_non_conformant_config 32781)
 (cl:defparameter glx_drawable_type 32784)
 (cl:defparameter glx_render_type 32785)
 (cl:defparameter glx_x_renderable 32786)
 (cl:defparameter glx_fbconfig_id 32787) (cl:defparameter glx_rgba_type 32788)
 (cl:defparameter glx_color_index_type 32789)
 (cl:defparameter glx_max_pbuffer_width 32790)
 (cl:defparameter glx_max_pbuffer_height 32791)
 (cl:defparameter glx_max_pbuffer_pixels 32792)
 (cl:defparameter glx_preserved_contents 32795)
 (cl:defparameter glx_largest_pbuffer 32796) (cl:defparameter glx_width 32797)
 (cl:defparameter glx_height 32798) (cl:defparameter glx_event_mask 32799)
 (cl:defparameter glx_damaged 32800) (cl:defparameter glx_saved 32801)
 (cl:defparameter glx_window 32802) (cl:defparameter glx_pbuffer 32803)
 (cl:defparameter glx_pbuffer_height 32832)
 (cl:defparameter glx_pbuffer_width 32833) (cl:defparameter glx_rgba_bit 1)
 (cl:defparameter glx_color_index_bit 2)
 (cl:defparameter glx_pbuffer_clobber_mask 134217728)
 (cl:defparameter glx_sample_buffers 100000)
 (cl:defparameter glx_samples 100001)
 (cl:defparameter glx_arb_render_texture 1))
