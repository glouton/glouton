(in-package :cl-user)

(require 'asdf)
(asdf:operate 'asdf:load-op 'verrazano)

(in-package :verrazano)
(defmethod translate-name (element)
  (let ((strans (assoc (named-name element) +special-names+ :test #'equal)))
    (when strans
      (setf (named-name element) (cdr strans))))
  (setf (named-name element)
	(string-downcase (named-name element))))

(generate-for ((par namespace-type) (edg allocates) (tgt cpp-type) bst)
  (when (not (get-note edg 'artificial))
    (cond
      ((constant-integer? tgt)
       (constant-integer-declaration edg (list par edg tgt) bst))
      (t (format t "Rejected ~A~%" (named-name edg))))))

(defun c-constant-to-lisp (str)
  (when (starts-with "0x" str)
    (setq str (concatenate 'string "#x" (subseq str 2 nil))))
  (when (ends-with "L" str)
    (setq str (subseq str 0 (1- (length str)))))
  str)

(defun create-macro-node (mac pstate curr-file)
  (let* ((op (car mac))
	 (sym (cadr mac))
	 (val (cddr mac))
	 (cval (convert-macro-value val))
	 (rns (gethash +root-namespace-id+ (parser-state-xnodes pstate))))
    (when (and cval (equal op "#define"))
      (let ((id (format nil "_VZN~A" (get-next-integer))))
	(setf (gethash id (parser-state-xnodes pstate))
	      `((:|Variable| :|id| ,id :|name| ,sym :|init| ,cval
		 :|context| "_1" :|file| ,curr-file
		 :|type| ,+const-integer-type-id+)))
	(set-element-attribute rns ':|members|
          (concatenate 'string 
		       (get-element-attribute rns ':|members|) " " id))))))

(defun concat-strings (strings)
  (let ((result-len 0))
    (loop
       for str in strings
       do (incf result-len (1+ (length str)))
       finally (decf result-len))
    (when (> result-len 0)
      (let ((result (make-string result-len)))
	(loop
	   for str in strings
	   for str-len = (length str)
	   and offset = 0 then (+ offset str-len 1)
	   do (progn
		(setf (subseq result offset) str)
		(when (< (+ offset str-len) result-len)
		  (setf (char result (+ offset str-len)) #\space))))
	result))))

(defun convert-macro-value (val)
  (unless (listp val)
    (return-from convert-macro-value nil))
  (when (eql (length val) 1)
    (handler-case (return-from convert-macro-value
		    (parse-number (c-constant-to-lisp (car val))))
      (t ()
	;; Keep on truckin'
	nil)))
  (let* ((expression (concatenate 'string (concat-strings val) ";"))
	 (val (handler-case (cparse::cparse-const-expression expression)
		(error ()
		  (return-from convert-macro-value nil)))))
    (if (and (typep val 'cparse::c-const) (typep val 'cparse::cinteger-super))
	(cparse::value val )
	nil)))

(in-package :cl-user)
(let ((*print-pretty* t))
  (loop for (binding . output-file) in '(("xlib.binding" . "xlib.lisp")
					 ("opengl2.binding" . "gl.lisp")
					 ;("glu.binding" . "glu.lisp")
					 ("glx.binding" . "glx.lisp"))
       do (verrazano:create-binding 
	   (verrazano:setup-build "gccxml" 
				  (make-pathname :device "/"
						 :directory "/tmp"))
	   binding
	   output-file
	   :cffi-backend)))

