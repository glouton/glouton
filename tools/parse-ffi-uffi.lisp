(in-package "CCL")

(require :parse-ffi)

(defun parse-ffi-uffi (inpath outpath &key (package-name))
  (let* ((*ffi-typedefs* (make-hash-table :test 'string= :hash-function 'sxhash))
         (*ffi-unions* (make-hash-table :test 'string= :hash-function 'sxhash))
         (*ffi-structs* (make-hash-table :test 'string= :hash-function 'sxhash))
         (argument-macros (make-hash-table :test 'equal)))
    (let* ((defined-types ())
           (defined-constants ())
           (defined-macros ())
           (defined-functions ())
           (defined-vars ()))
      (with-open-file (in inpath)
        (let* ((*ffi-ordinal* -1)
               (*ffi-prefix* (namestring inpath)))
          (let* ((*package* (find-package "KEYWORD")))
            (do* ((form (read in nil :eof) (read in nil :eof)))
                 ((eq form :eof))
              (case (car form)
                (:struct (push (process-ffi-struct form) defined-types))
                (:function (push (process-ffi-function form) defined-functions))
                (:macro (let* ((m (process-ffi-macro form))
                               (args (ffi-macro-args m)))
                          (if args
                            (setf (gethash (string (ffi-macro-name m)) argument-macros) args)
			    (push m defined-macros))))
                (:type (push (process-ffi-typedef form) defined-types))
                (:var (push (process-ffi-var form) defined-vars))
                (:enum-ident (push (process-ffi-enum-ident form) defined-constants))
                (:enum (process-ffi-enum form))
                (:union (push (process-ffi-union form) defined-types)))))
          (multiple-value-bind (new-constants new-macros)
              (process-defined-macros defined-macros (reverse defined-constants) argument-macros)
	    ;; If we're really lucky, we might be able to turn some C macros
	    ;; into lisp macros.  We can probably turn some C macros into
	    ;; lisp constants.
            (declare (ignore new-macros))
	    (let* ((*ffi-out* (when outpath
				(open outpath
				      :direction :output
				      :if-exists :supersede
				      :if-does-not-exist :create))))
	    
	      (unwind-protect
		   (let* ((*print-case* :downcase))
		     (when *ffi-out*
		       (format *ffi-out* "~%~%(cl::in-package ~a)~%~%" package-name))
		     (dolist (x (reverse new-constants))
		       (emit-ffi-constant (car x) (cdr x)))
                     (dolist (x (reverse defined-vars))
                       (emit-ffi-var (car x) (cdr x)))
		     (when *ffi-out*
		       (terpri *ffi-out*)
		       (terpri *ffi-out*))
		     (dolist (x (sort defined-types #'< :key #'ffi-type-ordinal))
		       (typecase x
			 (ffi-struct (define-struct x))
			 (ffi-union (define-union x))
			 (ffi-typedef (define-typedef x))))
		     (when *ffi-out*
		       (terpri *ffi-out*)
		       (terpri *ffi-out*))
		     (dolist (f (reverse defined-functions))
		       (emit-function-decl f)))
		(when *ffi-out* (close *ffi-out*))))
            outpath))))))

(defun emit-ffi-constant (name val)
  (record-global-constant name val)
  (when *ffi-out*
    (format *ffi-out* "~&(glouton-def:def-constant ~a " name)
    (format *ffi-out*
	    (typecase val
	      ((unsigned-byte 15) "~s)~&")
	      (unsigned-byte "#x~x)~&")
	      (t "~s)~&"))
	    val)))

(defun emit-struct (s)
  (when *ffi-out*
    (let ((name (ffi-struct-name s)))
      (if name
	  (progn
	    (when (ffi-struct-fields s)
	      (warn "Warning: struct reference ~S also has fields" name))
	    (format *ffi-out* "~&~vt(:struct ~s "
		    *ffi-indent* (escape-foreign-name name)))
	  (progn
	    (format *ffi-out* "~&~vt(:struct (" *ffi-indent*)
	    (emit-fieldlist (ffi-struct-fields s))
	    (format *ffi-out* ")"))))
    (format *ffi-out* ")")))

(defun define-struct (s)
  (unless (ffi-struct-defined s)
    (setf (ffi-struct-defined s) t)
    (record-global-struct s)
    (when (typep (ffi-struct-name s) 'keyword)
      (let* ((fields (ffi-struct-fields s)))
        (ensure-fields-defined fields)
	(when *ffi-out*
	  (format *ffi-out* "~&(glouton-def:def-struct ~a"
		  (unescape-foreign-name (ffi-struct-name s))) 
	  (let* ((*ffi-indent* (+ *ffi-indent* 2)))
	    (emit-fieldlist (ffi-struct-fields s)))
	  (format *ffi-out* ")~&"))))))

(defun emit-union (s)
  (when *ffi-out*
    (let ((name (ffi-union-name s)))
      (if name
	  (progn
	    (when (ffi-union-fields s)
	      (warn "Warning: union reference ~S also has fields" name))
	    (format *ffi-out* "~&~vt(:union ~s "
		    *ffi-indent* (escape-foreign-name name)))
	  (progn
	    (format *ffi-out* "~&~vt(:union (" *ffi-indent*)
	    (emit-fieldlist (ffi-union-fields s))
	    (format *ffi-out* ")"))))
    (format *ffi-out* ")")))

(defun define-union (u)
  (unless (ffi-union-defined u)
    (setf (ffi-union-defined u) t)
    (record-global-union u)
    (when (ffi-union-name u)
      (let* ((fields (ffi-union-fields u)))
        (ensure-fields-defined fields)
	(when *ffi-out*
	  (format *ffi-out* "~&(glouton-def:def-union ~a"
		  (unescape-foreign-name (ffi-union-name u)))
	  (let* ((*ffi-indent* (+ *ffi-indent* 2)))
	    (emit-fieldlist (ffi-union-fields u)))
	  (format *ffi-out* ")~&"))))))

(defun define-typedef (def)
  (unless (ffi-typedef-defined def)
    (setf (ffi-typedef-defined def) t)
    (record-global-typedef def)
    (let* ((target (ffi-typedef-type def)))
      (unless (and (consp target)
		   (member (car target) '(:struct :union :primitive)))
	(ensure-referenced-type-defined target))
      (when *ffi-out*
 	(format *ffi-out* "~&(glouton-def:def-foreign-type ~a "
		(unescape-foreign-name (ffi-typedef-name def)))
	(emit-type-reference target)
	(format *ffi-out* ")~&")))))

(defun translate-uffi-type (type)
  (if (consp type)
      (ecase (car type)
	(:signed
	 (ecase (cadr type)
	   (8 :char)
	   (16 :short)
	   (32 :int)
	   (64 :long-long)))
	(:unsigned
	 (ecase (cadr type)
	   (8 :unsigned-char)
	   (16 :unsigned-short)
	   (32 :unsigned-int)
	   (64 :unsigned-long-long)))
	(*
	 (if (eq (cadr type) t)
	     :pointer-void
	     (error "Pointer type ~s should not be primitive"))
	 ))
      type))

(defun emit-type-reference (ref)
  (when *ffi-out*
    (ecase (car ref)
      (:primitive (format *ffi-out* "~s" (translate-uffi-type (cadr ref))))
      (:typedef (format *ffi-out* "~a" (unescape-foreign-name
					(ffi-type-name (cadr ref)))))
      (:struct (let* ((s (cadr ref))
		      (name (ffi-struct-name s)))
		 (if (typep name 'keyword)
		   (format *ffi-out* "(:struct ~a)" (unescape-foreign-name
						     name))
		   (emit-struct s))))
      (:union (let* ((u (cadr ref))
		     (name (ffi-union-name u)))
		(if name
		  (format *ffi-out* "(:union ~a)" (unescape-foreign-name name))
		  (emit-union u))))
      (:pointer (let* ((target (cadr ref)))
		  (if (eq target *ffi-void-reference*)
		    (format *ffi-out* ":pointer-void")
		    (progn
		      (format *ffi-out* "(* ")
		      (emit-type-reference target)
		      (format *ffi-out* ")")))))
      (:array (format *ffi-out* "(:array ")
	      (emit-type-reference (caddr ref))
	      (format *ffi-out* " ~d)" (cadr ref))))
    (format *ffi-out* " ")))

(defun emit-function-decl (ffi-function)
  (let* ((args (ffi-function-arglist ffi-function))
         (retval (ffi-function-return-value ffi-function)))
    (if (eq (car (last args)) *ffi-void-reference*)
      (setq args (butlast args)))
    (when (ffi-record-type-p retval)
      (push retval args)
      (push `(:pointer ,retval) (ffi-function-arglist ffi-function))
      (setf (ffi-function-return-value ffi-function) *ffi-void-reference*)
      (setq retval *ffi-void-reference*))
    (dolist (arg args) (ensure-referenced-type-defined arg))
    (ensure-referenced-type-defined retval)
    (record-global-function ffi-function)
    (when *ffi-out*
      (format *ffi-out* "~&(glouton-def:def-function ")
      (emit-ffi-function-name-uffi (ffi-function-string ffi-function))
      (format *ffi-out* "~&  (")
      (loop
	 for arg in args
	 for i from 0
	 do (progn
	      (format *ffi-out* "(arg~d " i)
	      (emit-type-reference arg)
	      (format *ffi-out* ") "))
	 finally (format *ffi-out* ")~&  "))
      (format *ffi-out* ":returning ")
      (emit-type-reference retval)
      (format *ffi-out* ")~&"))))

(defun emit-ffi-function-name-uffi (name-string)
  (when *ffi-out*
    (if (some #'upper-case-p name-string)
      (format *ffi-out* "(~s ~a)" name-string (string-upcase name-string))
      (format *ffi-out* "~a" name-string))))